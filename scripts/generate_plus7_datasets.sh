#!/bin/bash

keep="yes"
if [ $1 -eq "keepmat" ]; then
	keep="no"
fi

# Create folds for each dataset
python ./scripts/split_cb513_KFolds.py 
python ./scripts/split_pisces_KFolds.py 

# Generate plus7 datasets
python ./scripts/create_casp13_ds.py
python ./scripts/create_cb513_ds.py 
python ./scripts/create_pisces_ds.py 

# Create .mat datasets
bash ./scripts/datasets2mat.sh ./generated_datasets/plus7_casp13
bash ./scripts/datasets2mat.sh ./generated_datasets/plus7_cb513
bash ./scripts/datasets2mat.sh ./generated_datasets/plus7_pisces

if [ $keep -eq "no" ]; then
	rm -rf ./generated_datasets/plus7_casp13
	rm -rf ./generated_datasets/plus7_cb513
	rm -rf ./generated_datasets/plus7_pisces
fi