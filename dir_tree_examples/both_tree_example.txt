.
├── CB513_cross_validation
│   ├── fold0
│   ├── fold1
│   ├── fold2
│   ├── fold3
│   ├── fold4
│   ├── fold5
│   ├── fold6
│   ├── fold7
│   ├── fold8
│   └── fold9
├── CB513_runAll.sh
├── CB513_test_pred
│   ├── fold0
│   ├── fold1
│   ├── fold2
│   ├── fold3
│   ├── fold4
│   ├── fold5
│   ├── fold6
│   ├── fold7
│   ├── fold8
│   └── fold9
├── CB513_train_pred
├── CB513_view_results.sh
├── PISCES_cross_validation
│   ├── fold1
│   ├── fold2
│   ├── fold3
│   ├── fold4
│   └── fold5
├── PISCES_runAll.sh
├── PISCES_test_pred
│   ├── fold1
│   ├── fold2
│   ├── fold3
│   ├── fold4
│   └── fold5
├── PISCES_train_pred
├── PISCES_view_results.sh
└── q3_sov_scripts
    ├── afterSOV.py
    ├── calc_Q3.py
    ├── ensembles.py
    ├── externalRules.py
    ├── prepare_SVM_files.py
    ├── runSOV.c
    ├── sov.c
    └── train_SVM.py

37 directories, 12 files
