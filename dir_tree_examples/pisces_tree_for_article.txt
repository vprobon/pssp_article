.
├── CASP13_cross_validation_for_PISCES
│   ├── fold0
│   │   └── pred_casp13_fold0_ex3.txt
│   ├── fold1
│   │   └── pred_casp13_fold1_ex2.txt
│   ├── fold2
│   │   └── pred_casp13_fold2_ex3.txt
│   ├── fold3
│   │   └── pred_casp13_fold3_ex2.txt
│   └── fold4
│       └── pred_casp13_fold4_ex3.txt
├── CASP13_pred_for_PISCES
│   ├── fold0
│   │   ├── pred_casp13_fold0_ex1.txt
│   │   ├── pred_casp13_fold0_ex10.txt
│   │   ├── pred_casp13_fold0_ex2.txt
│   │   ├── pred_casp13_fold0_ex3.txt
│   │   ├── pred_casp13_fold0_ex4.txt
│   │   ├── pred_casp13_fold0_ex5.txt
│   │   ├── pred_casp13_fold0_ex6.txt
│   │   ├── pred_casp13_fold0_ex7.txt
│   │   ├── pred_casp13_fold0_ex8.txt
│   │   └── pred_casp13_fold0_ex9.txt
│   ├── fold1
│   │   ├── pred_casp13_fold1_ex1.txt
│   │   ├── pred_casp13_fold1_ex10.txt
│   │   ├── pred_casp13_fold1_ex2.txt
│   │   ├── pred_casp13_fold1_ex3.txt
│   │   ├── pred_casp13_fold1_ex4.txt
│   │   ├── pred_casp13_fold1_ex5.txt
│   │   ├── pred_casp13_fold1_ex6.txt
│   │   ├── pred_casp13_fold1_ex7.txt
│   │   ├── pred_casp13_fold1_ex8.txt
│   │   └── pred_casp13_fold1_ex9.txt
│   ├── fold2
│   │   ├── pred_casp13_fold2_ex1.txt
│   │   ├── pred_casp13_fold2_ex10.txt
│   │   ├── pred_casp13_fold2_ex2.txt
│   │   ├── pred_casp13_fold2_ex3.txt
│   │   ├── pred_casp13_fold2_ex4.txt
│   │   ├── pred_casp13_fold2_ex5.txt
│   │   ├── pred_casp13_fold2_ex6.txt
│   │   ├── pred_casp13_fold2_ex7.txt
│   │   ├── pred_casp13_fold2_ex8.txt
│   │   └── pred_casp13_fold2_ex9.txt
│   ├── fold3
│   │   ├── pred_casp13_fold3_ex1.txt
│   │   ├── pred_casp13_fold3_ex10.txt
│   │   ├── pred_casp13_fold3_ex2.txt
│   │   ├── pred_casp13_fold3_ex3.txt
│   │   ├── pred_casp13_fold3_ex4.txt
│   │   ├── pred_casp13_fold3_ex5.txt
│   │   ├── pred_casp13_fold3_ex6.txt
│   │   ├── pred_casp13_fold3_ex7.txt
│   │   ├── pred_casp13_fold3_ex8.txt
│   │   └── pred_casp13_fold3_ex9.txt
│   └── fold4
│       ├── pred_casp13_fold4_ex1.txt
│       ├── pred_casp13_fold4_ex10.txt
│       ├── pred_casp13_fold4_ex2.txt
│       ├── pred_casp13_fold4_ex3.txt
│       ├── pred_casp13_fold4_ex4.txt
│       ├── pred_casp13_fold4_ex5.txt
│       ├── pred_casp13_fold4_ex6.txt
│       ├── pred_casp13_fold4_ex7.txt
│       ├── pred_casp13_fold4_ex8.txt
│       └── pred_casp13_fold4_ex9.txt
├── PISCES_cross_validation
│   ├── fold0
│   │   └── pred_test_fold0_ex3.txt
│   ├── fold1
│   │   └── pred_test_fold1_ex2.txt
│   ├── fold2
│   │   └── pred_test_fold2_ex3.txt
│   ├── fold3
│   │   └── pred_test_fold3_ex2.txt
│   └── fold4
│       └── pred_test_fold4_ex3.txt
├── PISCES_runAll.sh
├── PISCES_test_pred
│   ├── fold0
│   │   ├── pred_test_fold0_ex1.txt
│   │   ├── pred_test_fold0_ex10.txt
│   │   ├── pred_test_fold0_ex2.txt
│   │   ├── pred_test_fold0_ex3.txt
│   │   ├── pred_test_fold0_ex4.txt
│   │   ├── pred_test_fold0_ex5.txt
│   │   ├── pred_test_fold0_ex6.txt
│   │   ├── pred_test_fold0_ex7.txt
│   │   ├── pred_test_fold0_ex8.txt
│   │   └── pred_test_fold0_ex9.txt
│   ├── fold1
│   │   ├── pred_test_fold1_ex1.txt
│   │   ├── pred_test_fold1_ex10.txt
│   │   ├── pred_test_fold1_ex2.txt
│   │   ├── pred_test_fold1_ex3.txt
│   │   ├── pred_test_fold1_ex4.txt
│   │   ├── pred_test_fold1_ex5.txt
│   │   ├── pred_test_fold1_ex6.txt
│   │   ├── pred_test_fold1_ex7.txt
│   │   ├── pred_test_fold1_ex8.txt
│   │   └── pred_test_fold1_ex9.txt
│   ├── fold2
│   │   ├── pred_test_fold2_ex1.txt
│   │   ├── pred_test_fold2_ex10.txt
│   │   ├── pred_test_fold2_ex2.txt
│   │   ├── pred_test_fold2_ex3.txt
│   │   ├── pred_test_fold2_ex4.txt
│   │   ├── pred_test_fold2_ex5.txt
│   │   ├── pred_test_fold2_ex6.txt
│   │   ├── pred_test_fold2_ex7.txt
│   │   ├── pred_test_fold2_ex8.txt
│   │   └── pred_test_fold2_ex9.txt
│   ├── fold3
│   │   ├── pred_test_fold3_ex1.txt
│   │   ├── pred_test_fold3_ex10.txt
│   │   ├── pred_test_fold3_ex2.txt
│   │   ├── pred_test_fold3_ex3.txt
│   │   ├── pred_test_fold3_ex4.txt
│   │   ├── pred_test_fold3_ex5.txt
│   │   ├── pred_test_fold3_ex6.txt
│   │   ├── pred_test_fold3_ex7.txt
│   │   ├── pred_test_fold3_ex8.txt
│   │   └── pred_test_fold3_ex9.txt
│   └── fold4
│       ├── pred_test_fold4_ex1.txt
│       ├── pred_test_fold4_ex10.txt
│       ├── pred_test_fold4_ex2.txt
│       ├── pred_test_fold4_ex3.txt
│       ├── pred_test_fold4_ex4.txt
│       ├── pred_test_fold4_ex5.txt
│       ├── pred_test_fold4_ex6.txt
│       ├── pred_test_fold4_ex7.txt
│       ├── pred_test_fold4_ex8.txt
│       └── pred_test_fold4_ex9.txt
├── PISCES_train_pred
│   ├── pred_train_fold0_ex1.txt
│   ├── pred_train_fold1_ex1.txt
│   ├── pred_train_fold2_ex1.txt
│   ├── pred_train_fold3_ex1.txt
│   └── pred_train_fold4_ex1.txt
├── PISCES_view_results.sh
├── build_dir_tree.sh
├── final_results_CASP13_for_PISCES.txt
├── final_results_PISCES.txt
└── q3_sov_scripts
    ├── afterSOV.py
    ├── calc_Q3.py
    ├── ensembles.py
    ├── externalRules.py
    ├── prepare_SVM_files.py
    ├── runSOV.c
    ├── sov.c
    └── train_SVM.py

26 directories, 128 files
