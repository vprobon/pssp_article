[?1h=
                            < M A T L A B (R) >
                  Copyright 1984-2019 The MathWorks, Inc.
                   R2019a (9.6.0.1072779) 64-bit (maci64)
                               March 8, 2019

 
To get started, type doc.
For product information, visit www.mathworks.com.
 
    ----------------------------------------------------
	Your MATLAB license will expire in 14 days.
	Please contact your system administrator or
	MathWorks to renew this license.
    ----------------------------------------------------
[?1l>