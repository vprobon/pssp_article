> 6gnx_2
GYISIDAXKKFLGELHDFIPGTSGYLAYHVQN
CCEEHHHCCCCCCCEEECCCCCCCEEEEECCC
CCECHHHHHHHHCCCCCECCCCCCHEEEEECC
> 6fxa_1
ENIEETITVMKKLEEPRQKVVLDTAKIQLKEQDEQ
CHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCC
CCHHHHHHHHHHHHHHHHHHHECCCCCHHHHHHCC
> 5w9f_1
SQETRKKATEMKKKFKNBEVRADESNHCVEVRBSDTKYTLC
CCHHHHHHHHHHHHCCCCEEECCCCCCCEEEECCCCEEEEC
CEEEECCCEEEEEEEEEHHHHHHHHHHHHHHHHCCCCCCCC
> 6f45_2
SASIAIGDNDTGLRWGGDGIVQIVANNAIVGGWNSTDIFTEAGKHITSNGNLNQWGGGAIYCRDLNVS
CCEEECCCCCCEEEECCCCCEEEEECCEEEEEECCCEEEECCCCEEEECCCEEECCCCCEEECCEEEC
CCEEEECCCCCCCECCCCCEEEEEEHCCEEEEECCCCEEEECCCCECCCCCEECCCCCCEEEEEEECC
> 6qek_1
DIYGDEITAVVSKIENVKGISQLKTRHIGQKIWAELNILVDPDSTIVQGETIASRVKKALTEQIRDIERVVVHFEPAR
CHHHHHHHHHHCCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEEEEECC
CCCHHHHHHHHHCCCCCECCHHHEHHCCCCEEEEEEEEECCCCCCEHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCC
> 6qfj_1
SHMEDYIEAIANVLEKTPSISDVKDIIARELGQVLEFEIDLYVPPDITVTTGERIKKEVNQIIKEIVDRKSTVKVRLFAAQEEL
CHHHHHHHHHHHHHHCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEECCCEC
CCHHHHHHHHHHHHHCCCCCEECHHHHHHHCCCEEEEEEEEECCCCCCEHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCC
> 6btc_1
XNKKSKQQEKLYNFIIAKSFQQPVGSTFTYGELRKKYNVVCSTNDQREVGRRFAYWIKYTPGLPFKIVGTKNGSLLYQKIGINPC
CCCCCHHHHHHHHHHHHHHHHCCCCCEECHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECCCCCC
CCCCHHHHHHHHHHHHHEEEECCCCCEEEEEHHHHHCCCECCCHHHHHHHHHHHHEEHCCCCCCEEEECCCCCCEEEEECCCCCC
> 6d7y_1
LDTAQAPYKGSTVIGHALSKHAGRHPEIWGKVKGSXSGWNEQAXKHFKEIVRAPGEFRPTXNEKGITFLEKRLIDGRGVRLNLDGTFKGFID
CCCCCCEECCEEHHHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEECCCCCEEEEEECCCCCEEEEECCCCEEEEEC
CCHHCCCCCCCCHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCEEEEEECCCCCEEEECCCCCEEEEEC
> 6g57_1
KRSGFLTLGYRGSYVARIMVCGRIALAKEVFGDTLNESRDEKYTSRFYLKFTYLEQAFDRLSEAGFHMVACNSSGTAAFYRDDKIWSSYTEYIFFRP
CCCEEEEEEEEEECCCEEEEEEEHHHHHHHHHHHCCECCCCCCEEEEECCCCCHHHHHHHHHHCCCEEEEEEEEEEEECCCCCCEEEEEEEEEEEEC
CCCCEEEEECCCCEEEEEEEECCEEEEHHHHCCHHHCCCCCCCCCEEEECCCCHHHHHHHHHHCCCEEEEECCCCCCCCCCHHHHHHHEEEEEEECC
> 6msp_1
MGHHHHHHENLYFQSHMTDELLERLRQLFEELHERGTEIVVEVHINGERDEIRVRNISKEELKKLLERIREKIEREGSSEVEVNVHSGGQTWTFNEK
CCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEEECCEEEEEEEECCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECC
CCHHHHHHHHHHHHHHHHHHHCEECCCHHHHHHHCCCCCCCCCCCHCHHHCCCCCCCCCEECCCCCCCCCCCCCECCCCCCCCHCCCCCCCCHEECC
> 6gnx_1
SLKPFTYPFPETRFLHAGPNVYKFKIRYGNKEVITQELEDSVRVVLGNLDNLQPFATEHFIVFPYKSKWERVSHLKFKHGEIILIPYPFVFTLYVEXKW
CCCCCCCCCCEEEEEEECCEEEEEEEEECCHHHHHHHHHHHHHHHHHCCCCCCCEECCCEEEEEEEEECCCCCCEEEEECCEEEEEEEEEEEEEEEECC
CCCCCCCCCCCCEEEECCCCEEEEEEECCCCCCCHHHHHHHHEEEECCCCCCCCCCCCCEEECCCCCHHCCCCCCEEECCCCCCCCCCEEEEEEEECCC
> 6cp9_2
XFIENKPGEIELLSFFESEPVSFERDNISFLYTAKNKAGLSVDFSFSVVEGWIQYTVRLHENEILHNSIDGVSSFSIRNDNLGDYIYAEIITKELINKIEIRIRPDIKIKSSSVI
CCCCECCCHHHHHHHHCCCCCEEECCCCEEEEEEECCCCEEEEEEEECCCCEEEEEEEECCEEEEEEEEECCCECEEEEECCEEEEEEEEECCCCEEEEEEECCCCCEEEEEEEC
CCHECCCCHHHHHHHCCCCCEEECCCCCEEEEEECCCCCEEEEEEEECHHCCEEEEEEECCCEEEEEEHCCCEEEEEEECCCCCEEEEEEECCCCEEEEEEEECCCEEEEEEEEC
> 6cp9_1
TAQTIANSVVDAKKFDYLFGKATGNSHTLDRTNQLALEXKRLGVADDINGHAVLAEHFTQATKDSNNIVKKYTDQYGSFEIRESFFIGPSGKATVFESTFEVXKDGSHRFITTIPKNG
CHHHHHCCCCCHHHHHHHCCCCCCECCECCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHCCCHHHEEEEEECCCCEEEEEEEEEECCCCCEEEEEEEEEECCCCCEEEEEEEEECC
CHHCCCHEEECCCCCHHHECCCCCCCCCHHHHHHHHHHHHECCCCCCCCHHHHHHHHHHHHHHCCCCEEEEECCCCCCEEEEEEEECCCCCCCEEEEEEEECCCCCCCCEEEEECCCC
> 6d34_1
PASPNIEAILASYAGFRDRDIEGILSGMHPDVEWVHPEGMGKYGLGGTKLGHAGIKEFLAHVPTVLGGMRLAPREFIEQGDRVVVFGTREVTSLRGTTATLDFVHSWTMRDGKATRMEDIFDTVAFHELIES
CCCHHHHHHHHHHHHHHCCCHHHHHCCEEEEEEEEECHHHHHHCCCEEEEHHHHHHHHHHHHHHCEEEEEEEEEEEEEECCEEEEEEEEEEEECCCCEEEEEEEEEEEEECCEEEEEEEECCHHHHHHHHCC
CCCCCHHHHHHHHHHHHCCCHHHHHHHHCCCHEEEECCCCCCCCCCCCEECHHHHHHHHHHHHHHCCCEEECCHHEECCCCEEEEEEEEEEEECCCCCEHEHEEEEEEECCCCEEEEEHHHHHHHHHHHHHC
> 6rap_1
AITADDIAVQYPIPTYRFIVTLGDEQVPFTSASGLDINFDTIEYRDGTGNWFKMPGQRQAPNITLSKGVFPGKNAMYEWINAIQLNQVEKKDIMISLTNEAGTEVLVSWNVSNAFPTSLTSPSFDATSNEIAVQQITLMADRVTIQTA
CCCCCCHHHHCCCCCCCEEEEECCEECCCCEEECCCCCCCEEEEECCCCCEEEEECCCCCCEEEEEEECCCCCCCCHHHHCCCECCECCCEEEEEEEECCCCCCEEEEEEEEEEEEEEEECCCCCCCCCCCCEEEEEEECCCEEEEEC
CCCHHHHCCCCCCCCCEEEEEECCCCCEEEEECCCCEEEEEEEEECCCCCCEECCCCCCCCCEEEEECEECCCHHHHHHHHHHHCCCCCCCCEEEEEECCCCCCEEEEEEEECCCEEEECCCCCCCCCCHEEEEEHEEHHHCEEEECC
> 6d7y_2
XKELFEVIFEGVNTSRLFFLLKEIESKSDRIFDFNFSEDFFSSNVNVFSELLIDSFLGFNGDLYFGVSXEGFSVKDGLKLPVVLLRVLKYEGGVDVGLCFYXNDFNSAGKVXLEFQKYXNGISADFGFENFYGGLEPASDQETRFFTNNRLGPLL
CCEEEEEEEECCCCCCHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCCCHHHHHHHHHCCCCEEEEEEEEEEEEECCEEEEEEEEEEEEECCEEEEEEEEEECCHHHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCHHHEEEECCEECCCC
CHHHHEEEEECCCCCCHHHHHHHHCCCCCCCCCCCCCCCCHCCCHHHHHHHHHHHHCCCCCCEEEEEEECCCCCCCEEECCEEEEEEEECCCCEEEEECCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCEEECCCCCCCCCCEEEECCCCCCCCC
> 6cp8_3
VNSTAKDIEGLESYLANGYVEANSFNDPEDDALECLSNLLVKDSRGGLSFCKKILNSNNIDGVFIKGSALNFLLLSEQWSYAFEYLTSNADNITLAELEKALFYFYCAKNETDPYPVPEGLFKKLXKRYEELKNDPDAKFYHLHETYDDFSKAYPLNN
CCCHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHCCCCCHHHHCHHHHHHHHHHHCCCCC
CCCHHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHCHHHHHHHHEEEECCCCCCCHHHHHHHHHHHHCHCCHHHHHHHHHHHHHCCCHHHHHHHHHHHEECCCCCCCCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCC
> 6cp8_1
NSFEVSSLPDANGKNHITAVKGDAKIPVDKIELYXRGKASGDLDSLQAEYNSLKDARISSQKEFAKDPNNAKRXEVLEKQIHNIERSQDXARVLEQAGIVNTASNNSXIXDKLLDSAQGATSANRKTSVVVSGPNGNVRIYATWTILPDGTKRLSTVTGTFK
CCCEEEEEECCCCCEEEEEEECCEEEECHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHCCCCECCECEEEEEEEECCEEEEEEEEEEECCCCCEEEEEEECCCC
CCCEEEEECCCCCCEEEEEECCCCEECCCEEEEEECCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHCCCCCCCCEEEEEEECCCCCEEEEEEEEECCCCCCEEEEECCCCC
> 6e4b_1
AMRLWLIRHGETQANIDGLYSGHAPTPLTARGIEQAQNLHTLLHGVSFDLVLCSELERAQHTARLVLSDRQLPVQIIPELNEMFFGDWEMRHHRDLMQEDAENYSAWCNDWQHAIPTNGEGFQAFSQRVERFIARLSEFQHYQNILVVSHQGVLSLLIARLIGMPAEAMWHFRVDQGCWSAIDINQKFATLRVLNSRAIGVEN
CEEEEEEECCCEHHHHHCECCCCCCCCECHHHHHHHHHHHHHCCCCCCCEEEECCCHHHHHHHHHHCCCCCCCEEECHHHCCCCCHHHCCCEHHHHHHHCHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHHHHHHCCCCHHHHHHECCCCCCEEEEEEECCEEEEEEEEECCCCCCC
CCEEEEEECCCCHHHHHCCECCCCCCCCCHHHHHHHHHHHHHHHCCCEEEEECCCCHHHHHHHHHHHHCCCCCEEECHHHHHCCCCCECCCCHHHHHHHCHHHHHHHHCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCHHHHHHHHHHCCCHHHHHCCCCCCCCEEEEEECCCCCEEEEECCCCEEECC
> 6rbk_1
SLLERGLSKLTLNAWKDREGKIPAGSMSAMYNPETIQLDYQTRFDTEDTINTASQSNRYVISEPVGLNLTLLFDSQMPGNTTPIETQLAMLKSLCAVDAATGSPYFLRITWGKMRWENKGWFAGRARDLSVTYTLFDRDATPLRATVQLSLVADESFVIQQSLKTQSAPDRALVSVPDLASLPLLALSAGGVLASSVDYLSLAWDNDLDNLDDFQTGDFLRAT
CCCCCCCCCCEEEEECCCCCCCEEEEEECCCCCCCEEEEEEEEEEECCCCCCCCCCEEEEEEEEEEEEEEECCECCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCCCEEECCCCCCCCCCEECCCEEECCCCCCCECCCCCECECCEEEEECCCCCCCHHHHHCCCCCCCCCCEECCCCCCHHHHHHHHHHHCCCCCCHHHHHHHCCCCCCCCCCCCCEECCC
CHHHHHHHHEEEEECCCCCCCCCCCEEEEEECCCHEEEEEEEEECCCCCCCCCCCCCEECCCCCCEEEEEEEEECCCCCCCCCHHHHHHHHHHEECCCCCCCCCCEEEEEECCECCCCCCCEEEEEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCHHHCCCCCCCCCCCEEEEEECCCCCCHHHHHHHHHHCCHHHHHHHHHHHCCCCCCCCCCCCEEEEC
> 6f45_1
AVQGPWVGSSYVAETGQNWASLAANELRVTERPFWISSFIGRSKEEIWEWTGENHSFNKDWLIGELRNRGGTPVVINIRAHQVSYTPGAPLFEFPGDLPNAYITLNIYADIYGRGGTGGVAYLGGNPGGDCIHNWIGNRLRINNQGWICGGGGGGGGFRVGHTEAGGGGGRPLGAGGVSSLNLNGDNATLGAPGRGYQLGNDYAGNGGDVGNPGSASSAEMGGGAAGRAVVGTSPQWINVGNIAGSWL
CCCCCCHHHHHHHHHCCCEHHHHHHHCCCCCCCEEHHHHCCCCCCEEEEECCCEEEECHHHHHHHHHHCCCCCEEEEECCCEEECCCCCCCEEECCCCCCCCEEEEECCEEECCCCCCCECCCCCCCCCCCEEECCHHHEEEEECCEEECCCCCCCCEEECCEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEECCEECCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCEEEECCEEECCCC
CCECCCCCCCCCHCCCCCHHHHHHCEEEEECCCCHHHHHCCCCCCCCCCECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
> 6mxv_1
IXQHSSGFLKLVDDAKSRIQECSVDDIQKXNETQTLDGLLIDTREESEVANGYIPNAIHLSKGIIESAIESAVPNKNQKXYFYCGGGFRSALVADKLREXGYKNVISVDGGWRAWNAKGYPTVSPNQFRPNEFLKLVNNAKTQIKECSTTELYNKINSQELDGIVFDVREDSEFNRFHIQGATHLSKGQIEVKIENLVPNKQQKIYLYCGSGFRSALAAESLQHXGYTNVVSIAGGIKDWLANNYPVSQN
CCCCCHHHHHHHHHHHCCCEEECHHHHHHHHHHCCCCCEEEECCCHHHHCCCECCCEEECCCCCHHHHHHHHCCCCCCCEEEECCCCCHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECHHHCCCHHHHHHHHHHHHHCEEECHHHHHHHHHCCCCCCEEEECCCHHHHHHCECCCCEECCCCCHHHHHHHHCCCCCCCEEEECCCCHHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECC
CCCCCCCHHHHHHHHHHCCCCCCHHHHHHHHHHCCCCEEEEECHCHHHHCCCCCCCCEECCCCCCHHHHHHCCCCCCCEEEEECCCCCHHHHHHHHHHHCCCCEEEEEHCCHHHHHHCCCCECCCCCCCCHHHHHHHHHHHCCCCCCCHHHHHHHHHCCCCCEEEEECCCHHHHHHCCCCCCEECCCHHHHHHHHHCCCCCCCEEEEEECCCCHEHHHHHHHHHCCCCEEEEHHCCHHHHHHCCCCCCCC
> 5z82_1
SIGLAHNVTILGSGETTVVLGHGYGTDQSVWKLLVPYLVDDYKVLLYDHMGAGTTNPDYFDFDRYSSLEGYSYDLIAILEEFQVSKCIYVGHSMSSMAAAVASIFRPDLFHKLVMISPTPRLINTEEYYGGFEQKVMDETLRSLDENFKSLSLGTAPLLLACDLESAAMQEYCRTLFNMRPDIACCITRMICGLDLRPYLGHVTVPCHIIQSSNDIMVPVAVGEYLRKNLGGPSVVEVMPTEGHLPHLSMPEVTIPVVLRHIRQDITD
CHHHHCCCEEEECCCCEEEEECCCCCCHHHHCCCHHHCCCCCEEEEECCCCCCCCCHHHCCCCHHHCCHHHHHHHHHHHHHCCCCCEEEEEECHHHHHHHHHHHHCHHHEEEEEEECCCCCCECCCCCCCCECHHHHHHHCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCHHHHHHCCCCEEEEEEEECCCCCHHHHHHHHHHCCCCEEEEEEEEECCCHHHHCHHHHHHHHHHHHHCCCCC
CHHHHCCEEEECCCCCEEEEECCCCCCHHHHHEECHHHHHCCEEEEEEECCCCCCCCCHCCCCHHCCCHCHHHHHHHHHHHHCCCCEEEEECCCHHHHHHHHHHHCHHHHCEEEEEECCCEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHCCHEEECCCCCHHHHHHHHHHHHCCCHHHHHHHHEEEECCCCHHCCCCCCCCEEEEECCCCEECCEHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCHHHHHHHHHHHHHHHCC
> 6rap_5
SNYQTLVDVNNAMNKMLRAYVNEAVAIRFDLPDTQADAAISVFLYDIHEDLQLRTAESRGFNAGAGRLLPGWVNVKCNYLITYWESPDSQPDNQAIQVMSQVLAALINNRQLADIGAYTQVMPPKENLNSLGNFWQSLGNRPRLSLNYCVTVPISLSDKGEEMTPVKSLSTTVEPKAPLSPLVITDALREQLRVALDACLAMTHVNLDSSPVANSDGSAAEIRVSLRVYGMTPTEYLAPMNTVFNEWEKSEAAAVTPDGYRVYINAVDKTDLTGI
CCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCEEECCCCECHHHCCCCCCCCCCCCCCCCCCCEEEECCEEECCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCCCCEEECCCCCCCHHHHHHHHHHCCCCCCCCEEEEEEEEECCCCCCCCCCECCCEEEEEEECCCCCHHHHHHHHHHHHHHHCCHHHHHHHECCEEEEECCCCCCCCCCEEEEECEECCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCEECCCCCCECCCC
CCHHHHHHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCEEEEEEECHHHCHCCCCCCCCCCCCCCCCCCCCCEECCHEEEEEECCCCCCCCCHHHHHHHHHHHHHHCCCCCCCHCCCCCEECCCCCCCCCHHHHHHHCCCCCCCEEEEEEEEEECCCCCCCCCCCEEEEEEEECCCCCCCCHHHHHHHHHHHHHHHHHHHHHHECCCCCCCCCCCCCCCCCCEEEEEECCCCCHHHHHHHHHHHHHHCCCCEEEEEECCCEEEEEECCCCCCCCC
> 6d2v_1
GLVPRGSHMEGKKILVTGGTGQVARPVAEALAERNEVWCLGRFGTPGVEKELNDRGITTFHWDMDDPGAAAYEGLPDDFTHVLHSAVRRGEDGDVNAAVEVNSVACGRLMTHCRGAEAFLFVSTGALYKRQTLDHAYTEDDPVDGVADWLPAYPVGKIAAEGAVRAFAQVLNLPTTIARLNIAYGPGGYGGVPMLYFKRMLAGEPIPVPKEGQNWCSLLHTDDLVAHVPRLWEAAATPATLVNWGGDEAVGITDCVRYLEELTGVRARLVPSEVTRETYRFDPTRRREITGPCRVPWREGVRRTLQALHPEHLPS
CCCCHHHHCCCCEEEEECCCCCCHHHHHHHHHCCCEEEEEECCCCCCHHHHHHHCCCEEEECCCCCCCHHHHCCCCCCCCEEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEEEHHHECCCCCCCEECCCCCECCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCEECCCCCCEHHHHHHHHHHHCCCEEEECCCCCEECCEEHHHHHHHHHHHHHCCECCCEEEEECCCCCEEHHHHHHHHHHHHCCCCCEEEECCCCCCCEECCHHHHHHHCCCCCCHHHHHHHHHHHHCHHHCCC
CCCCCCCCCCCCEEEEECCCCCEEHHHHHHHHHCCCEEEEECCCCHHHHHHHHCCCCEEEEECCCCHHHHHHHCCCCCCCEEEEEHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHEEEECCCCECCCCCCCCCCCECCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCCCCCCCCHHHHHHHHHCCCCEEECCCCCCCCCCCEEHHHHHHHHHHHHCCCCCCEEEEECCCCCEEHHHHHHHHHHHCCCCCCECCCCCCCCCCCCHHHHHHHCCCCCCCCHHHHHHHHHHHHHCCCCCC
> 6q64_1
LIPLIEAQTEEDLTPTXREYFAQIREYRKTPHVKGFGWFGNWTGKGNNAQNYLKXLPDSVDFVSLWGTRGYLSDEQKADLKFFQEVKGGKALLCWIIQDLGDQLTPKGLNATQYWVEEKGQGNFIEGVKAYANAICDSIEKYNLDGFDIDYQPGYGHSGTLANYQTISPSGNNKXQVFIETLSARLRPAGRXLVXDGQPDLLSTETSKLVDHYIYQAYWESSTSSVIYKINKPNLDDWERKTIITVEFEQGWKTGGITYYTSVRPELNSXEGNQILDYATLDLPSGKRIGGIGTYHXEYDYPNDPPYKWLRKALYFGNQVYPGKFD
CCCHHHCCCHHHCCHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCHHHCHHHCCCCCCEEECCCCCCCCCHHHHHHHHHHHHCCCCEEEEEEEECCCCCCCCCCCCCHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCCCCCCCCCECCCCCHHHHHHHHHHHHHHCCCCCEEEEEECHHHECHHHHCCCCCEEEECCCCCEHHHHHHHHCCCCCCCHHHHEEEEEEHHHHCCCCCCCCCECCCHHHHHCCCHHHHHHHCCECCCCCECCEEEEECHHHHCCCCCCCHHHHHHHHHHHHHCCCCCC
CCCCCCCCCHCCCCHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCCCCCCCHHCCCCCCCEEEEECCCCCCCCHHHHHHHHHHHHCCCCEEEEEEEHHHCCCCCCCCCCCCHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCCHHHHHHHHCEEEEECCCCCCCCCCCCCCCCCCCCCCCCEEEEEECCHHHHCCCCCCCCCCCCCCCCCCCCHHHHHHHHCCCCCCCCCCCEEEEHHHHCCCCCCCCHHHHHHHHHHCCCCCCCCC
> 6n91_1
MITSSLPLTDLHRHLDGNIRTQTILELGQKFGVKLPANTLQTLTPYVQIVEAEPSLVAFLSKLDWGVAVLGDLDACRRVAYENVEDALNARIDYAELRFSPYYMAMKHSLPVTGVVEAVVDGVRAGVRDFGIQANLIGIMSRTFGTDACQQELDAILSQKNHIVAVDLAGDELGQPGDRFIQHFKQVRDAGLHVTVHAGEAAGPESMWQAIRDLGATRIGHGVKAIHDPKLMDYLAQHRIGIESCLTSNLQTSTVDSLATHPLKRFLEHGILACINTDDPAVEGIELPYEYEVAAPQAGLSQEQIRQAQLNGLELAFLSDSEKKALLAKAALRG
CCCCCCCCEEEEEEHHHCCCHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHCCEEEEEEEEEHHHCHHHHHHHHHHHHCCHHHCCEEEEECCCCCCCHHHHHHHHHHHHHCCCEEEEEECCCCCHHHHHHHHHHCCCCEEEECHHHHHCHHHHHHHHHHCCEEEECHHHHHHCCCCCCHHHCCHHHHHHCCCCEEECCECHHHHCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHCCC
CCCCCCCHHHEEEECCCCCCHHHHHHHHHHCCCCCCCCCHHHHHHHEEECCCCCCHHHHHHHHCCCEEECCCHHHHHHHHHHHHHHHHHHCCEEEEEEHCHHHHHHCCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEECCCCCCHHHHHHHHHHHHHCCCCEEEEECCCCCCCCCCHHHHHHHHHHHHCCCCEEEECCCCCCHHHHHHHHHHHCCEECCCCEEEHCCHHHHHHHHHCCCEEEECCCCCCECCCHHCHHHCHHHHHHHCCCEEEEECCCCCCCCCEHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHCCC
> 6cvz_1
KHKYHFQKTFTVSQAGNCRIMAYCDALSCLVISQPSPQASFLPGFGVKMLSTANMKSQYIPMHGKQIRGLAFSSYLRGLLLSASLDNTIKLTSLETNTVVQTYNAGRPVWSCCWCLDEANYIYAGLANGSILVYDVRNTSSHVQELVAQKARCPLVSLSYMPRAASAAFPYGGVLAGTLEDASFWEQKMDFSHWPHVLPLEPGGAIDFQTENSSRHCLVTYRPDKNHTTIRSVLMEMSYRLDDTGNPICSCQPVHTFFGGPTAKLTKNAIFQSPENDGNILVCTGANSALLWDAASGSLLQDLQTDQPVLDICPFEVNRNSYLATLTEKMVHIYKWE
CCCEEEEEEEECCCCCCCCCEEEECCCCEEEEEEECCCCCCCCCEEEEEEECCCCCCEEEEEECCCEEEEEECCCCCCEEEEEECCCEEEEEECCCCEEEEEEECCCCEEEEEECCCCCCEEEEEECCCCEEEEECCCCCCCCEEECCCCCCCCEEEEEEECCCCCCCCCCCEEEEEECCEEEEEEECCCCCEEEEECCCCEEEEEEEEEECCCCEEEEEEEECCCCCCCEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCCCCCECEEEECCCCCCCEEEEECCCEEEEEECCCCCEEEEEECCCCCCEEEEEEECCEEEEEEECCCEEEEEEEC
CCEEEEEEEEEEECCCCCEEEEECCCCCEEEEECCCCCCCCCCCCCEEEECCCCCCCEEECCCCCCCCEEEECCCCCCEEEEEHCCCCEEEEECCCCEEEEEEECCCCCEEEECCCCCCCEEEEECCCCEEEEECCCCCCCCCHECCCCCCCCCEEEEEECCCCCCCCCCCCEEEEECCCCCEEEECCCCCCCCCCCCCCCCCCCEEEECCCCCCCEEEEECCCCCCCCCCCEECCCCCCCCCCCCCCCCCCEEEEECCCCCCCCCCCEEEECCCCCCEEEEECCCCCEEEEECCCCCEEEECCCCCCEEEEEEECCCCCCEEEEECHHHEEEEECC
> 6ek4_1
VVYPEINVKTLSQAVKNIWRLSHQQKSGIEIIQEKTLRISLYSRDLDEAARASVPQLQTVLRQLPPQDYFLTLTEIDTELEDPELDDETRNTLLEARSEHIRNLKKDVKGVIRSLRKEANLMASRIADVSNVVILERLESSLKEEQERKAEIQADIAQQEKNKAKLVVDRNKIIESQDVIRQYNLADMFKDYIPNISDLDKLDLANPKKELIKQAIKQGVEIAKKILGNISKGLKYIELADARAKLDERINQINKDCDDLKIQLKGVEQRIAGIEDVHQIDKERTTLLLQAAKLEQAWNIFAKQLQNTIDGKIDQQDLTKIIHKQLDFLDDLALQYHSMLLS
CCCCCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHCCCECCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCC
CCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCC
> 6rap_3
TTTYPGVYLSEDAVSSFSVNSAATAVPLFAYDSENTNTINKPIQVFRNWAEFTVEYPTPLEDAFYTSLSLWFMHGGGKCYLVNEANIADAVAQYDDITLIVAAGTDTTTYTAFTTVVGQGYRIFGLFDGPKEKIAGTAKPDEVMEEYPTSPFGAVFYPWGTLASGAAVPPSAIAAASITQTDRTRGVWKAPANQAVNGVTPAFAVSDDFQGKYNQGKALNMIRTFSGQGTVVWGARTLEDSDNWRYIPVRRLFNAVERDIQKSLNKLVFEPNSQPTWQRVKAAVDSYLHSLWQQGALAGNTPADAWFVQVGKDLTMTQEEINQGKMIIKIGLAAVRPAEFIILQFSQDI
CCCCCCCCCCCCCCCCCCCCCCCCCCCEEEECCCCCCCCCCCEEEECCHHHHHHHCCCCCCCCCHHHHHHHHCCCCCCEEEEECCCHHHHHHHHCCCCCEEECCCCCCHHHHHHHHHHCCCCCCEECCCCCCCCCCCCCHHHCCCCCCCCCCEECCCCCECCCCCCCECHHHHCCHHHHHHHCCCCCCCCCCCCECCCCCECCCCCHHHHCCCCCCCCCCEEECCCCCCCEEECCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCHHHCCCECCHHHHHHHHHHHHHHHHHHHHCCCECCCCHHHHEEEECCCCCCCCHHHHCCCCEEEEEEECECCEECEEEEEEECCC
CCCCCCEEEEECCCCCCCCCCCCCCEEEEEECCCCCCCCCCCCEEECCHHHHHHHHCCCCCHHHHHHHHHHHHCCCCCCEEECCCCCHHHHHHHCCCEEEECCCCCHHHHHHHHHHHHHHCCEEEEECCCCCCCCCCCCHHHHCCCCCCCHHHHHHCCHHHCCCEEECCCCHHHHEEEEEECCCCCEEECCCEEEHCCCCCCEEECCHHHHHCCCCCCEEEEECCCCCCEEEEEEEECCCCCCEEEEEHHHHHHHHHHHHHHHCCEEEECCCCCHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHEEEEECCCCCCCHHHHHCCCEEEEEEEECCCCCEEEEEEECCCC
> 6cci_1
VELPPEEADLFTGEWVFDNETHPLYKEDQBEFLTAQVTCMRNGRRDSLYQNWRWQPRDASLPKFKAKLLLEKLRNKRMMFVGDSLNRNQWESMVBLVQSVVPPGRKSLNKTGSLSVFRVEDYNATVEFYWAPFLVESNSDDPNMHSILNRIIMPESIEKHGVNWKGVDFLVFNTYIWWMNTFAMKVLRGSFDKGDTEYEEIERPVAYRRVMRTWGDWVERNIDPLRTTVFFASMSPLHIKSLDWENPDGIKDALETTPILNMSMPFSVGTDYRLFSVAENVTHSLNVPVYFLNITKLSEYRKDAHTSVHTIRQGKMLTPEQQADPNTYADDIHWCLPGLPDTWNEFLYTRIISR
CCCCCCCCCCCCEEEEECCCCCCCCCHHHCCCCCCCCCCCCCCCCCCHHHHEEEEECCCCCCCCCHHHHHHHCCCCEEEEEECHHHHHHHHHHHHHHHCCCCCCCEEEEEECCEEEEEECCCCEEEEEEECCCCCCECCCCCCCCCCCCCEECCCCCHHHHHHHCCCCEEEECCCHHHCCCCEEEEECCCHHHCCCCEEEEEHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCHHHHHCCCCCEEECCCEECCEECCHHHHCCHHHHCEEEEECCCCHHHHHHHHHHHHHHCC
CECCCCCCCCCCCEEEEECCCCCCCCCCCCCCCCCHHHHHHCCCCCCHHHHCCCCCCCCCCCCHCHHHHHHHHCCCEEEEECCHHCCCCEEEHHEHHHCCCCCCCCCCCCCCCEEEEEEHHCCCEEEHHECCEEEECCCCCCCCCCCCCHEEECCCHHHHHHCCCCCEEEEEEEEEHHCCCCCEEEEECCCCCCCCEEEEECHHHHHHHHHHHHHHHHHHCCCCCCEEEEEEECCCCCCCCHHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCEEEEEEHHHHHHHCCCCCCEEEHCCCCCCCHHHHCCCCCHCHHHHEECCCCCCHHHHHHHHHHHCC
> 6cl6_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKEADGELPGGVNLDSMVTSGWWSQSFTAQAASGANYPIVRAGLLHVYAASSNFIYQTYQAYDGESFYFRCRHSNTWFPWRRMWHGGDFNPSDYLLKSGFYWNALPGKPATFPPSAHNHDVGQLTSGILPLARGGVGSNTAAGARSTIGAGVPATASLGASGWWRDNDTGLIRQWGQVTCPADADASITFPIPFPTLCLGGYANQTSAFHPGTDASTGFRGATTTTAVIRNGYFAQAVLSWEAFGR
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHHCECCCCCCCEEEEEEEEECCEEEEEEEECCCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCHHHCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEECCCEEEEEECCCCCCCCEEEEEEEECCCCCCCCCCCEEEECCCCCEEEEEECCCCCEEEEEEEEEC
CHHHCCCHHHHHHHHHCCCHCHCCCCHCCHHHHHHHCCCCCCCHCCCCCEEECCCCCCCCCCCCCHHHCCCCCCCCCHHHHHHHHCCCHHHHHHHHHCCCCCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCHHHCCCCCEEECCCCHHHCCCCCCCCCHHHHEEEEECCCCCEEEEEEECCCCCEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCEEEEECEEECCCCCCCEEEEEECCCCCCEEEEEEECCCCCCCCCCCCEECCCCCCEEEEECCCCCCCCEEEEEECC
> 6cl5_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKTTDGSIGNGVNINSFVNSGWWLQSTSEWAAGGANYPVGLAGLLIVYRAHADHIYQTYVTLNGSTYSRCCYAGSWRPWRQNWDDGNFDPASYLPKAGFTWAALPGKPATFPPSGHNHDTSQITSGILPLARGGLGANTAAGARNNIGAGVPATASRALNGWWKDNDTGLIVQWMQVNVGDHPGGIIDRTLTFPIAFPSACLHVVPTVKEVGRPATSASTVTVADVSVSNTGCVIVSSEYYGLAQNYGIRVMAIGY
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHCCECCCCCCCEEEEEEEEECCEEEEEEEECCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCCCCCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEEECCCCCCEEEEEECCCCCCCCEEEEEEEEEEEECCCCCHHHEEEEEEEECCCEEEEEEEECCCCCCEEEEEEEEEEC
CHHCCCCHHHHHHHHHCCHHHHHCCCHHHHHHHHHCCCCCCCCHCCCCHHEECCCCCCCCCCHCHHCCCCCCCCCCCHHHEEEECCCCCHHHHHHHHCCCCCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCHHHCCCEEECCCCHHHCCCCCCCCHCCCEEEEEECCCCEEEEEEECCCCCEEEEECCCCCCCCHECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHEEEEEEEECCCCCCCCCCCEEEEEECCCCCCCEEEEEECCCCCCCCCCCHHEEECCCCCCCCEEEEECCCCCCCCCCCEEEEEECC
> 6hrh_1
LYFQSMFSYDQFFRDKIMEKKQDHTYRVFKTVNRWADAYPFAQHFSSKDVSVWCSNDYLGMSRHPQVLQATQETLQRHGVGAGGTRNISGTSKFHVELEQELAELHQKDSALLFSSCFVANDSTLFTLAKILPGCEIYSDAGNHASMIQGIRNSGAAKFVFRHNDPDHLKKLLEKSNPKIPKIVAFETVHSMDGAICPLEELCDVSHQYGALTFVDEVHAVGLYGSRGAGIGERDGIMHKIDIISGTLGKAFGCVGGYIASTRDLVDMVRSYAAGFIFTTSLPPMVLSGALESVRLLKGEEGQALRRAHQRNVKHMRQLLMDRGLPVIPCPSHIIPIRVGNAALNSKLCDLLLSKHGIYVQAINYPTVPRGEELLRLAPSPHHSPQMMEDFVEKLLLAWTAVGLPLQCRRPVHFELMSEWERSYFGNM
CCCCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEECCCCCCEEEEECCEEEEECCCCCCCCHHHCHHHHHHHHHHHHHHCCCCCCCCCCCCECHHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHHHHHCCCCEEEEECCCCHHHHHHHHHHCCEEEEECCCCHHHHHHHHHCCCCCCCEEEEEECECCCCCCECCHHHHHHHHHHCCEEEEEECCCCCCCCCCCCCCHHHHCCCHHHCCEEEEECCCCCCCCCEEEEECHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCCEEEECCCHHHHHHHHHHHHHCCCEECCEECCCCCCCCCCEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHCCCC
CCCCCCCCHHHHHHHHHHHHHCCCCEEEEHHHHHHCCCCCCCCCCCCCCEEEEECCCHHECCCCHHHHHHHHHHHHHCCCCCCCCEECCCCCHHHHHHHHHHHHHHCHHHEEEECHHEHHHHHHHHHHHHHCCCCEEECCCCCCCHHHHHHHCCCCCEEEEECCCHHHHHHHHHHCCCCCCEEEEEEEEECCCCCCCCHHHHHHHHHHCCCEEEEECHEEEECECCCCCCECCHCCCCCCEEEEHCCCCCCCCCEEEEHHCCHHHHHHHHCCCCCEEEEECCCHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEEEEECCCHHHHHHHHHHHHHCCCEEEECCCCCCCCCHEEEEECCCCCCCCHHHHHHHHHHHHHHHHHCCCHHCCCCHHHHHHHHHHHHHCCCC
> 6m9t_1
DCGSVSVAFPITMLLTGFVGNALAMLLVSRSYRRRESKRKKSFLLCIGWLALTDLVGQLLTTPVVIVVYLSKQRWEHIDPSGRLATFFGLTMTVFGLSSLFIASAMAVERALAIRAPHWYASHMKTRATRAVLLGVWLAVLAFALLPVLGVGQYTVQWPGTWAFISTNWGNLFFASAFAFLGLLALTVTFSCNLATIKALVSRGSNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLNAAKSELDKAIGRNTNGVITKDEAEKLFNQDVDATVRGILRNAKLKPVYDSLDAVRRAALINMVFQMGETGVAGFTNSLRMLQQKRWDEAAVNLAKSRWYNQTPNRAKRVITTFRTGTWDAYGSWGRITTETAIQLMAIMCVLSVCWSPLLIMMLKMIFNEKQKECNFFLIAVRLASLNQILDPWVYLLLRKILGRPLEVL
CCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHHHHHHHHHHHCHHHHCCCCEEEECCCCEEEECCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHCCEEEEEECCCCCEEEECCEEEECCCCHHHHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHCCHHHHHHHHHHHHHHCHHHHHCCHHHHHHHHCCCHHHHHHHHHCCHHHHHCHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHC
CCCCCCEEEEHHEHHHHHHHHHHHHHHHHHHHHCHCCCCCCEEEEEEHHHHHHHHHHHEECCHEEEEEHHHCCCCCCCCCCCHHHHHCEEEEEHHHHCHHHHHHHHHHHHHEECCCCCHCCCHCCHHHHHHHHHHHHHHHHHHHHHHHCCCCCEEEECCCCEEEECCCCCHHHHHHHHHHHHHHHHHHHHHEHHEEEHEHHHHHHCCCCCCCCCCCCHHHHHHHHHEEEEEEEEEEEEEEEECCEEEEEHCCCCCCCCCCHHHHHHHHHHHHHCCCCHEEEHECHHHHHHHHHHCCCCCCCCCCCCEEECCCCCCHCCHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCHHEEEHCCCCCCCCCCCCCCEEHHHHHHEEEEEEEEEECCCCHHEEEEEEECCCCCCCHCHHEHHHHHHCCHHHHCCHEEEHECHHHHHHHHCC
> 5w6l_1
NAQELKERAKVFAKPIGASYQGILDQLDLVHQAKGRDQIAASFELNKKINDYIAEHPTSGRNQALTQLKEQVTSALFIGKXQVAQAGIDAIAQTRPELAARIFXVAIEEANGKHVGLTDXXVRWANEDPYLAPKHGYKGETPSDLGFDAKYHVDLGEHYADFKQWLETSQSNGLLSKATLDESTKTVHLGYSYQELQDLTGAESVQXAFYFLKEAAKKADPISGDSAEXILLKKFADQSYLSQLDSDRXDQIEGIYRSSHETDIDAWDRRYSGTGYDELTNKLASATGVDEQLAVLLDDRKGLLIGEVHGSDVNGLRFVNEQXDALKKQGVTVIGLEHLRSDLAQPLIDRYLATGVXSSELSAXLKTKHLDVTLFENARANGXRIVALDANSSARPNVQGTEHGLXYRAGAANNIAVEVLQNLPDGEKFVAIYGKAHLQSHKGIEGFVPGITHRLDLPALKVSDSNQFTVEQDDVSLRVVYDDVANKPKITFKG
CCCCHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCHHHCCCCCCCCCCCCCCCCCCCEEEECCCCHHHHHHHHHHHHHCCCCCCEEEECCCCEEEECCCHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHECHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHCCCCCCCHHHHHHHCCCHHHHHHHHHCCCCEEEECCCCCCCCHHHHHHHHCHHHHHHHCEEEEEECCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHCCCCHHHHHHHHHCCCEEEECCCHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCHHHHCCEEECCCEECCHHHHHCCCEEEECCCCCEEECCCCHHHCCECCCCCCCCCCCCCC
CCHHHHHHHHECCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCEEEEEECCCHHHHHHHHHHHCCCCCCCCEEECCCCCEEEECCCHHHHHHCCCCCHHCHHHHHHHHHHHCCCCCCCCHHHHHHHHHCCCCCHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCEEEECCCCCCCCHHHHHHHHHHHHHHCCCCEEEEHHCHHHHHHHHHHHHHCCCCCCHHHHHHHCCCCCHHHHHHHHHHCCCEEEEECCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCCCCCCCCHHHHCCCEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCEEECC
> 6rbk_3
HITLDIAGQRSTLGIRRLRVQQLINEIPLAQLELHIPTDNHGAADNAVQHEVSRFTLGVRVGIAQDNKPLFDGYLVQKKMQLKGKEWSVRLEARHALQKLTFLPHSRVFRQQDDSTVMKGLLQSAGVKLTQSKHDQLLQFRLSDWQFIRSRLLSTNCWLLPDAASDTVVIRPLSSRTLARDSHDYTLYEINLNFDNRFTPDSLSLQGWDIAAQRLTAAQKSPAGAFRPWKPAGQDYALAFSMLPEATLQTLSNSWLNYQQMTGVQGHIVLAGTRDFAPGESITLSGFGAGLDGTAMLSGVNQQFDTQYGWRSELVIGLPASMLEPAPPVRSLHIGTVAGFTADPQHLDRIAIHLPALNLPDSLIFARLSKPWASHASGFCFYPEPGDEVVVGFIDSDPRYPMILGALHNPKNTAPFPPDEKNNRKGLIVSQADQTQALMIDTEEKTLRLMAGDNTLTLTGEGNLTMSTPNALQLQADTLGLQADSNLSIAGKQQVEITSAKINM
CEEEEECCECCCCCEEEEEEEECCCCCCEEEEEEECCCCCECHHHHCCHHHHCCCCCCCEEEEEECCEECCCEEEEEEEECCCCCCCEEEEEEECHHHHHCCCCECCCCCCCCHHHHHCCCHHHCCCEEECCCCCCCCCCCECCCCCCCCCCCCCCEECCECCCCCEEECEECCCCCECCCCCCCCEEEEEECCCCCCCCCCCEEEECCCCCCCCCCCECCCCCCCCCEEECCCCCEEEECCCCCCCCHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCEEECCCCCCCCEEECEEEEECCEECCCCECCEEEECCCCCCCCCCCCCCCCEEECECCCCCCCCCCCCEEECCCCCCCCCCCEEEEECCCCCCCCCCCCCCCCCCCCEEEEEHHHCCCCEEEEECCCCCCCCCCCCCCCCCCECCCCCECCCCECCEECECCCCCCEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CEEECCCCCCCCEEEEEEEEHHHCCCCCCEEEEEEECCCCCCCCCCCHHCCCCCCCEEEEECCCCCCEEEEEEEEEEEEEEECCCCCEEEEEECCHHHHHHHCCCCEEEHCCCHHHHHHHHHHHCCCCCCCCCCHEEEECCCCCHHHHHHHHHHCCEEEEECCCCEEEECCCCCCCCCEEECCCCCHEEHHHHHCHHCCHHHEEEECCCCHHCHHEEEECCCCCCCCCCCCCHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHEEEEEECCCCCCCCCCEEEECCCCCCCCCCEEEEEEEEEECCCCCEEEEEEEECCCCCCCCCCCCCCCEEEEEECCCCCCCCCCEEEEEEEECCCCCCCEEEEEEECCCCCCCCEEECCCCCCEEEEEECCCCCCCCEEEECCCCCCCCCCCCCCCCCCCEEEEEECCCCCEEEEECCCCCEEEEECCCCEEEEECCCCEEEECCCEEEEECCCEEEECCCCEEEEECCCEEEEECEECC
> 6n9y_1
MERFLRKYNISGDYANATRTFLAISPQWTCSHLKRNCLFNGMCAKQNFERAMIAATDAEEPAKAYRLVELAKEAMYDRETVWLQCFKSFSQPYEEDIEGKMKRCGAQLLEDYRKNGMMDEAVKQSALVNSERVRLDDSLSAMPYIYVPIKEGQIVNPTFISRYRQIAYYFYSPNLADDWIDPNLFGIRGQHNQIKREIERQVNTCPYTGYKGRVLQVMFLPIQLINFLRMDDFAKHFNRYASMAIQQYLRVGYAEEVRYVQQLFGKIPTGEFPLHHMMLMRRDFPTRDRSIVEARVRRSGDENWQSWLLPMIIVREGLDHQDRWEWLIDYMDRKHTCQLCYLKHSKQIPTCGVIDVRASELTGCSPFKTVKIEEHVGNNSVFETKLVRDEQIGRIGDHYYTTNCYTGAEALITTAIHIHRWIRGCGIWNDEGWREGIFMLGRVLLRWELTKAQRSALLRLFCFVCYGYAPRADGTIPDWNNLGSFLDTILKGPELSEDEDERAYATMFEMVRCIITLCYAEKVHFAGFAAPACESGEVINLAARMSQMRMEY
CHHHHHHHCCCHHHHHHHHHHHHHCCCCCCCCCCHHHECCCCECCCCHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCHHHHHHCCCCCCCCCCCCCCCCEECHHHHCEEECCCCECCCCEEEECCCCEEEEECCCCCCEEECCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCEEEECCCHHHHCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCCCHHHHCCCCCCCCCCCCHHHHHHCCCCCCCHHHCCEECHHHHHHCCCCCCHHHHHHHHHHCCCCCHHHHHCCCCCCEEEEECHHHHHHCCCCCEEEEEEEEECCCCCCCCCCCCCCEEEEEECCEEEEEECCCCCHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCC
HHHHHHHCCCCHHHHHHHHHHHHHCCCCECCCCCCEEECCCHEHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHCHHHHHHHHCCCCCCCCEEECCCHCCCCEEEEEECCCCCCCCCEHHCCCCEEEEEECCCCCCCCCCCCCHHHHHHHHHHHHHHHHCCCCCCCCCCCCCEEEEEEEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHCECCCCCCHHHHHHHCCCCCCCCCCHHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCCCHHHHHHHCCCCCHHHHHHHHHHCCCCEEEEEEEEHHHHCCCCHHHEEEECECCCCCCCEEEEECCCCEEEECCCEEEEEECCCCHHHHEEHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHEEECCCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCEEEECCCCCCCCCCCHHHHHHHHHHHHHHC
> 6nq1_1
AARWDLCIDQAVVFIEDAIQYRSINHRVDASSMWLYRRYYSNVCQRTLSFTIFLILFLAFIETPSSLTSTADVRYRAAPWEPPAGLTESVEVLCLLVFAADLSVKGYLFGWAHFQKNLWLLGYLVVLVVSLVDWTVSLSLVAHEPLRIRRLLRPFFLLQNSSMMKKTLKCIRWSLPEMASVGLLLAIHLCLFTMFGMLLFAGLTYFQNLPESLTSLLVLLTTANNPDVMIPAYSKNRAYAIFFIVFTVIGSLFLMNLLTAIIYSQFRGYLMKSLQTSLFRRRLGTRAAFEVLSSMVGAVGVKPQNLLQVLQKVQLDSSHKQAMMEKVRSYGSVLLSAEEFQKLFNELDRSVVKEHPPRPEYQSPFLQSAQFLFGHYYFDYLGNLIALANLVSICVFLVLDADVLPAERDDFILGILNCVFIVYYLLEMLLKVFALGLRGYLSYPSNVFDGLLTVVLLVLEISTLAVYRLSLWDMTRMLNMLIVFRFLRIIPSMKPMAVVASTVLGLVQNMRAFGGILVVVYYVFAIIGINLFRGVIVALSAPBGSFEQLEYWANNFDDFAAALVTLWNLMVVNNWQVFLDAYRRYSGPWSKIYFVLWWLVSSVIWVNLFLALILENFLHKW
CHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHCCHHHHHHHHHHHHHHCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHCCCHHHHHHCCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHCCCCCCCCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCECHHHHHHHHHHCCCCHHHHHHHHHHHHHCCCCCECHHHHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCHHHCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCC
CCCHHHHHHHHHEEHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHEHCHHHHHHCHCHEEEEEEEEEEHHHHHEEEECCCCCCEEEHHHHCEEEEEHCCHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHEEEECCCCCCEEEECCCHCHHHEEEEEEEEEEEEHHHHHHHHHEEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHECCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHCCCEEECCCCCCCCCCCHHHHHHHHEECCHHHEEEEEEEHHHHEEEEEEEEHCCCCCCCCCCCCEEHHHHHHEHHHHHHHHHHHHHHCCCHHHHCCCCCCCCEEEEEHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCHHHEHHHHHHHHHHHHHHHCCEEEEEEEHHHHEEHHHHCCCCCCCCCCCCCHHHHHHHHHCHHHHHHHHHEHEEEEEECCCEEEHHHHHHHCCCHHEEEEEEEEEEEEEEEEEEEEEHHHHHHHECC
> 6dru_1
TYFAPNSTGLRIQHGFETILIQPFGYDGFRVRAWPFRPPSGNEISFIYDPPIEGYEDTAHGMSYDTATTGTEPRTLRNGNIILRTTGWGGTTAGYRLSFYRVNDDGSETLLTNEYAPLKSLNPRYYYWPGPGAEFSAEFSFSATPDEQIYGTGTQQDHMINKKGSVIDMVNFNSYIPTPVFMSNKGYAFIWNMPAEGRMEFGTLRTRFTAASTTLVDYVIVAAQPGDYDTLQQRISALTGRAPAPPDFSLGYIQSKLRYENQTEVELLAQNFHDRNIPVSMIVIDYQSWAHQGDWALDPRLWPNVAQMSARVKNLTGAEMMASLWPSVADDSVNYAALQANGLLSATRDGPGTTDSWNGSYIRNYDSTNPSARKFLWSMLKKNYYDKGIKNFWIDQADGGALGEAYENNGQSTYIESIPFTLPNVNYAAGTQLSVGKLYPWAHQQAIEEGFRNATDTKEGSAADHVSLSRSGYIGSQRFASMIWSGDTTSVWDTLAVQVASGLSAAATGWGWWTVDAGGFEVDSTVWWSGNIDTPEYRELYVRWLAWTTFLPFMRTHGSRTBYFQDAYTBANEPWSYGASNTPIIVSYIHLRYQLGAYLKSIFNQFHLTGRSIMRPLYMDFEKTDPKISQLVSSNSNYTTQQYMFGPRLLVSPVTLPNVTEWPVYLPQTGQNNTKPWTYWWTNETYAGGQVVKVPAPLQHIPVFHLGSREELLSGNVF
CCCCCCCCCEEEEECCEEEEEEECCCCEEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCCCEEEEECCEEEEEECCCCCCCCCECEEEEECCCCCEEEEEEECCCCCCCCCEEEECCCCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCCCEEECCCECCEEEEEEEEECCCEEEEECCCCCEEEEECCCEEEEEEEEECCEEEEEEECCCCCHHHHHHHHHHHHCCCCCCCHHHHCEEECCCCCCCHHHHHHHHHHHHHCCCCCCEEEECCCCECCCCCCCECCCCCCCHHHHHHHHHHHHCCEEEEEECCEECCCCCCHHHHHHCCCECEECCCCCCCEEECCEEEEEECCCCHHHHHHHHHHHHHHCHHHCCCEEEECCCCCCCCCCCCCCCCCCHHHHHCCCCCCCEECCCCEHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEECCCCCCHHHCCEEEECCCCECCHHHHHHHHHHHHHHHHHCCCCEECCECCCECCCCCCCCCECCCHHHHHHHHHHHHHHCCCCCEEECCCCECCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCHHHHHHHHHHHHHHCCCCEECCHHHHCCCCCCHHHHHHCCCHHHHCCEEECCCEEECCCCCCCCCEEEEEECCCCCCCCCCEEECCCCCEECCCEEEEEECCCCCCCEEECCCHHHHCCCCCC
CEEECCCCEEEEECCCCEEEEECCCCCEEEEEECCCCCCCCCCCCECCCCCCCCCCCCCCCCCCEEEECCCCCEEEECCCEEEEECCCCCCCCCCEEEEEECCCCCCHEEEHHHCCCCCCCCCHHECCCCCCCCEEEEEEECCCCCHHHECCCCCCCCCCCCCCCEEEHHHCCCCEEEEEEECCCCCEEEEECCCCCEEEECCCCCEEEHHHHHHEEEEEECCCCCCHHHHHHHHHHHCCCCCCCCCCCCCCEHHHHHHCCHHHHHHHHHHHHHCCCCCEEEEEEEECCCCCCCCCCCCCCCCCCHHHHHHHHHHCCCEEEEEEECCCCCCCHCHHHHHHHCCEEECCCCCCECCCCCCCCEEEECCCCCHHHHHHHHHHHHHHHHHCCEEEEECHCCCCCCCCCCCCCCCCCCEECCCCCCCCCEEECCCCHHHCCHHHHHHHHHHHHHHHCCCCCCCCCCCCEEEEEEHHHCCCCHCEEEEECCCCCCCHHHHHHHHHHHHHHHHCCCCEEECCCCCEECCCCCCCCCCCCCCHHHHHHHHHHHHHHHEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCHHCCCCCCCCHHHCCCCCCCCCCCHHHCCCHHEECCCCCCCCCEEEEECCCCCCCCCCCEEEECCCCEECCCEEEEECCCCCEEEEEEECCCCCCCCCCCC
