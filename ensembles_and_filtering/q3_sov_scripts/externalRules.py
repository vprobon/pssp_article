import sys

class externalRules:
    def applyRules(filename, outSOV, outPred):
        f = open(filename, "r")
        lines = f.readlines()
        f.close()
        f = open(outSOV, "w")
        f1 = open(outPred, "w")

        for i in range(0, len(lines), 4):
            f1.write(lines[i])
            f1.write(lines[i + 1])
            f1.write(lines[i + 2])
            f.write(">OSEQ\n")
            f.write(lines[i + 2])
            f.write(">PSEQ\n")
            j = 0
            lines[i + 3] = list(lines[i + 3].translate({ord(c):'' for c in ' \n\t\r'}))
            # print(len(lines[i + 3]))
            while j < len(lines[i + 3]):
                if len(lines[i + 3]) - j >= 4:
                    if lines[i + 3][j] == 'H' and lines[i + 3][j + 1] == 'E' and lines[i + 3][j + 2] == 'E' and \
                                    lines[i + 3][j + 3] == 'H':
                        lines[i + 3][j] = 'H'
                        lines[i + 3][j + 1] = 'H'
                        lines[i + 3][j + 2] = 'H'
                        lines[i + 3][j + 3] = 'H'
                        j += 4
                        continue
                    if lines[i + 3][j] != 'H' and lines[i + 3][j + 1] == 'H' and lines[i + 3][j + 2] == 'H' and \
                                    lines[i + 3][j + 3] != 'H':
                        lines[i + 3][j + 1] = 'C'
                        lines[i + 3][j + 2] = 'C'
                        j += 4
                        continue
                if len(lines[i + 3]) - j >= 3:
                    if lines[i + 3][j] == 'H' and lines[i + 3][j + 1] == 'E' and lines[i + 3][j + 2] == 'H':
                        lines[i + 3][j + 1] = 'H'
                        j += 3
                        continue
                j += 1

            if lines[i + 3][0] == 'E' and lines[i + 3][1] != 'E':
                f.write("C")
                f1.write("C")
            elif lines[i + 3][0] == 'H' and lines[i + 3][1] != 'H':
                f.write("C")
                f1.write("C")
            else:
                f.write(lines[i + 3][0])
                f1.write(lines[i + 3][0])

            for j in range(1, len(lines[i + 3]) - 1):
                if lines[i + 3][j - 1] != 'E' and lines[i + 3][j] == 'E' and lines[i + 3][j + 1] != 'E':
                    f.write("C")
                    f1.write("C")
                    continue
                elif lines[i + 3][j - 1] != 'H' and lines[i + 3][j] == 'H' and lines[i + 3][j + 1] != 'H':
                    f.write("C")
                    f1.write("C")
                    continue
                f.write(lines[i + 3][j])
                f1.write(lines[i + 3][j])

            if lines[i + 3][len(lines[i + 3]) - 1] == 'E' and lines[i + 3][len(lines[i + 3]) - 2] != 'E':
                f.write("C")
                f1.write("C")
            elif lines[i + 3][len(lines[i + 3]) - 1] == 'H' and lines[i + 3][len(lines[i + 3]) - 2] != 'H':
                f.write("C")
                f1.write("C")
            else:
                f.write(lines[i + 3][len(lines[i + 3]) - 1])
                f1.write(lines[i + 3][len(lines[i + 3]) - 1])

            f.write('\n')
            f1.write('\n')
            f.write(">AA\n")
            f.write(lines[i + 1])

    applyRules(sys.argv[1].replace(',', ''), sys.argv[2].replace(',', ''), sys.argv[3])
    # print('End of external rules script\n')
