from numpy import *
import string as string
import sys


class Ensembles:
    def run(filenames, windowSize, ensemble, outPred, outSOV, outWeka):
        f = open(outPred, "w")
        files = open(filenames, "r").readlines()
        files = [w.replace('\n', '') for w in files]
        files = [open(i, "r") for i in files]
        i = 0
        LABELS = ['C', 'E', 'H', '!']
        if ensemble == 1:
            for rows in zip(*files):
                if i == 3:
                    for j in range(0, len(rows[0].translate(str.maketrans('', '', string.whitespace))), 1):
                        count = [0, 0, 0, 0]
                        for k in range(0, len(rows), 1):
                            if rows[k][j] == 'C':
                                count[0] += 1
                            elif rows[k][j] == 'E':
                                count[1] += 1
                            elif rows[k][j] == 'H':
                                count[2] += 1
                            else:
                                count[3] += 1
                        f.write(LABELS[argmax(count)])
                    f.write('\n')
                    i = 0
                else:
                    f.write(rows[0])
                    i += 1
            f.close()
        else:
            print('ERROR!!! Invalid ensemble option.')

        # count accuracy
        f = open(outPred, "r")
        lines = f.readlines()
        f.close()
        count = 0
        countall = 0
        for i in range(0, len(lines), 4):
            for j in range(0, len(lines[i + 2].translate(str.maketrans('', '', string.whitespace))), 1):
                if lines[i + 2][j] == lines[i + 3][j]:
                    count += 1
                countall += 1

        print('Accuracy: ' + str(float(count) / float(countall) * 100) + '%')

        # Confusion Matrix
        countHH = 0
        countHE = 0
        countHC = 0
        countEH = 0
        countEE = 0
        countEC = 0
        countCH = 0
        countCE = 0
        countCC = 0
        countH = 0
        countE = 0
        countC = 0
        countHp = 0
        countEp = 0
        countCp = 0
        for i in range(0, len(lines), 4):
            for j in range(0, len(lines[i + 2].translate(str.maketrans('', '', string.whitespace))), 1):
                if lines[i + 2][j] == 'H' and lines[i + 3][j] == 'H':
                    countHH += 1
                elif lines[i + 2][j] == 'H' and lines[i + 3][j] == 'E':
                    countHE += 1
                elif lines[i + 2][j] == 'H' and lines[i + 3][j] == 'C':
                    countHC += 1
                elif lines[i + 2][j] == 'E' and lines[i + 3][j] == 'H':
                    countEH += 1
                elif lines[i + 2][j] == 'E' and lines[i + 3][j] == 'E':
                    countEE += 1
                elif lines[i + 2][j] == 'E' and lines[i + 3][j] == 'C':
                    countEC += 1
                elif lines[i + 2][j] == 'C' and lines[i + 3][j] == 'H':
                    countCH += 1
                elif lines[i + 2][j] == 'C' and lines[i + 3][j] == 'E':
                    countCE += 1
                elif lines[i + 2][j] == 'C' and lines[i + 3][j] == 'C':
                    countCC += 1

                '''if lines[i + 2][j] == 'H':
                    countH += 1
                elif lines[i + 2][j] == 'E':
                    countE += 1
                elif lines[i + 2][j] == 'C':
                    countC += 1

                if lines[i + 3][j] == 'H':
                    countHp += 1
                elif lines[i + 3][j] == 'E':
                    countEp += 1
                elif lines[i + 3][j] == 'C':
                    countCp += 1'''

        print('\n\t\tCONFUSION MATRIX\n')
        print('{0:10}{1:10}{2:10}{3:10}'.format(' ', 'H', 'E', 'C'))
        print('{0:1}{1:10d}{2:10d}{3:10d}'.format('H', countHH, countHE, countHC))
        print('{0:1}{1:10d}{2:10d}{3:10d}'.format('E', countEH, countEE, countEC))
        print('{0:1}{1:10d}{2:10d}{3:10d}'.format('C', countCH, countCE, countCC))

        # SOV input file
        # f = open(outPred, "r")
        f1 = open(outSOV, "w")
        # lines = f.readlines()
        # f.close()

        for i in range(0, len(lines), 4):
            f1.write('>OSEQ\n')
            f1.write(lines[i + 2])
            f1.write('>PSEQ\n')
            f1.write(lines[i + 3])
            f1.write('>AA\n')
            f1.write(lines[i + 1])
        f1.close()

        # weka input file
        f1 = open(outWeka, "w")
        f1.write('@RELATION secondary_structure\n\n')
        for i in range(0, windowSize * 2 - 1, 1):
            f1.write('@ATTRIBUTE aminoacid' + str(i) + ' {C,E,H,0.0}\n')
        f1.write('@ATTRIBUTE output {C,E,H}\n')
        f1.write('\n@DATA\n')

        leadingzeros = zeros((1, (windowSize - 1)))
        for i in range(3, len(lines), 4):
            line = leadingzeros
            line = append(line, list(lines[i].rstrip()))
            line = append(line, leadingzeros)
            for j in range(0, len(lines[i].rstrip()), 1):
                for k in range(0, windowSize * 2 - 1, 1):
                    f1.write(str(line[j + k]) + ',')
                f1.write(lines[i - 1].rstrip()[j] + '\n')

        f1.close()

    files = sys.argv[1].replace(',', '')
    run(files, int(sys.argv[2].replace(',', '')), int(sys.argv[3].replace(',', '')), sys.argv[4].replace(',', ''),
        sys.argv[5].replace(',', ''), sys.argv[6])
    # print('\nEnd of ensembles script\n')
