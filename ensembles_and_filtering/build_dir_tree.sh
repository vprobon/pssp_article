#!/bin/bash

if [ $# -eq 0 ]; then
	echo "No arguments given, using default option: ALL"
	echo "Building directory tree for CB513 and PISCES..."
	mkdir -p {CB513_cross_validation/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CB513_test_pred/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CB513_train_pred,PISCES_cross_validation/{fold0,fold1,fold2,fold3,fold4},PISCES_test_pred/{fold0,fold1,fold2,fold3,fold4},PISCES_train_pred,CASP13_cross_validation_for_CB513/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CASP13_pred_for_CB513/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CASP13_cross_validation_for_PISCES/{fold0,fold1,fold2,fold3,fold4},CASP13_pred_for_PISCES/{fold0,fold1,fold2,fold3,fold4}}
elif [ $# -eq 1 ]; then
	if [[ "$1" != CB513 && "$1" != PISCES && "$1" != ALL ]]; then
		echo "Invalid option provided. Available options: CB513, PISCES, ALL"
		exit 1
	fi
	if [ "$1" = CB513 ]; then
		echo "Building directory tree for CB513..."
		mkdir -p {CB513_cross_validation/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CB513_test_pred/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CB513_train_pred,CASP13_cross_validation_for_CB513/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CASP13_pred_for_CB513/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9}}
	elif [ "$1" = PISCES ]; then 
		echo "Building directory tree for PISCES..."
		mkdir -p {PISCES_cross_validation/{fold0,fold1,fold2,fold3,fold4},PISCES_test_pred/{fold0,fold1,fold2,fold3,fold4},PISCES_train_pred,CASP13_cross_validation_for_PISCES/{fold0,fold1,fold2,fold3,fold4},CASP13_pred_for_PISCES/{fold0,fold1,fold2,fold3,fold4}}
	else
		echo "Building directory tree for CB513 and PISCES..."
		mkdir -p {CB513_cross_validation/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CB513_test_pred/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CB513_train_pred,PISCES_cross_validation/{fold0,fold1,fold2,fold3,fold4},PISCES_test_pred/{fold0,fold1,fold2,fold3,fold4},PISCES_train_pred,CASP13_cross_validation_for_CB513/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CASP13_pred_for_CB513/{fold0,fold1,fold2,fold3,fold4,fold5,fold6,fold7,fold8,fold9},CASP13_cross_validation_for_PISCES/{fold0,fold1,fold2,fold3,fold4},CASP13_pred_for_PISCES/{fold0,fold1,fold2,fold3,fold4}}
	fi
else
	echo "Invalid number of arguments given. Available options: CB513, PISCES, ALL"
	exit 1
fi