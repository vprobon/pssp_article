> 6gnx_2
GYISIDAXKKFLGELHDFIPGTSGYLAYHVQN
CCEEHHHCCCCCCCEEECCCCCCCEEEEECCC
CCCCHHHHHEEEEEEECCCCCCCCHEEEEECC
> 6fxa_1
ENIEETITVMKKLEEPRQKVVLDTAKIQLKEQDEQ
CHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHC
> 5w9f_1
SQETRKKATEMKKKFKNBEVRADESNHCVEVRBSDTKYTLC
CCHHHHHHHHHHHHCCCCEEECCCCCCCEEEECCCCEEEEC
CCHHCCCCCCCCCCCHEHHCCHHHHHHHHCCHCHCCEECCC
> 6f45_2
SASIAIGDNDTGLRWGGDGIVQIVANNAIVGGWNSTDIFTEAGKHITSNGNLNQWGGGAIYCRDLNVS
CCEEECCCCCCEEEECCCCCEEEEECCEEEEEECCCEEEECCCCEEEECCCEEECCCCCEEECCEEEC
CCEEEECCCCCCCCCCCCCEEEEEECCEEEEEECCCCEEEECCCEECCCCCEEECCCCCEEEEEECCC
> 6qek_1
DIYGDEITAVVSKIENVKGISQLKTRHIGQKIWAELNILVDPDSTIVQGETIASRVKKALTEQIRDIERVVVHFEPAR
CHHHHHHHHHHCCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCEEEEEEEEEECC
CCCHHHHHHHHHCCCCEEEHHHEEEECCCCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCC
> 6qfj_1
SHMEDYIEAIANVLEKTPSISDVKDIIARELGQVLEFEIDLYVPPDITVTTGERIKKEVNQIIKEIVDRKSTVKVRLFAAQEEL
CHHHHHHHHHHHHHHCCCCCCEEEEEEEEEECCEEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEECCCEC
CCCHHHHHHHHHHHHCCCCEEEHHHHEHHHHCCHEEEEEEEEECCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCCCCC
> 6btc_1
XNKKSKQQEKLYNFIIAKSFQQPVGSTFTYGELRKKYNVVCSTNDQREVGRRFAYWIKYTPGLPFKIVGTKNGSLLYQKIGINPC
CCCCCHHHHHHHHHHHHHHHHCCCCCEECHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECCCCCC
CCCCHHHHHHHHHHHHHHEEECCCCCEEEEHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCEEEEECCCCCC
> 6d7y_1
LDTAQAPYKGSTVIGHALSKHAGRHPEIWGKVKGSXSGWNEQAXKHFKEIVRAPGEFRPTXNEKGITFLEKRLIDGRGVRLNLDGTFKGFID
CCCCCCEECCEEHHHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHHHCCCCCEEEECCCCCEEEEEECCCCCEEEEECCCCEEEEEC
CCHCCCCCCCCCHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCEEEEEECCCCCEEEECCCCCEEEEEC
> 6g57_1
KRSGFLTLGYRGSYVARIMVCGRIALAKEVFGDTLNESRDEKYTSRFYLKFTYLEQAFDRLSEAGFHMVACNSSGTAAFYRDDKIWSSYTEYIFFRP
CCCEEEEEEEEEECCCEEEEEEEHHHHHHHHHHHCCECCCCCCEEEEECCCCCHHHHHHHHHHCCCEEEEEEEEEEEECCCCCCEEEEEEEEEEEEC
CCCCEEEEEECCCCCCEEEECCHHHHHHHHCCHHHHHCCCCCCCCEEEECCCHHHHHHHHHHHCCCEEEEECCCCCCHHCHHHHHCCCCEEEEEECC
> 6msp_1
MGHHHHHHENLYFQSHMTDELLERLRQLFEELHERGTEIVVEVHINGERDEIRVRNISKEELKKLLERIREKIEREGSSEVEVNVHSGGQTWTFNEK
CCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCCCCCEEEEEEEECCEEEEEEEECCCHHHHHHHHHHHHHHHHHCCCCCEEEEEEECCEEEEEECC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHCHHHHHHHHHHHHCHCCHHCCCCEECCCCCCCCCEEECCECC
> 6gnx_1
SLKPFTYPFPETRFLHAGPNVYKFKIRYGNKEVITQELEDSVRVVLGNLDNLQPFATEHFIVFPYKSKWERVSHLKFKHGEIILIPYPFVFTLYVEXKW
CCCCCCCCCCEEEEEEECCEEEEEEEEECCHHHHHHHHHHHHHHHHHCCCCCCCEECCCEEEEEEEEECCCCCCEEEEECCEEEEEEEEEEEEEEEECC
CCCCCCCCCCCCEEEEECCEEEEEEEECCCCCEEEHHHHHHHHHHHHCCCCCCCCCCCCEEECCCCCHHCCCCCCCEECCCCEECCCCEEEEEEEECCC
> 6cp9_2
XFIENKPGEIELLSFFESEPVSFERDNISFLYTAKNKAGLSVDFSFSVVEGWIQYTVRLHENEILHNSIDGVSSFSIRNDNLGDYIYAEIITKELINKIEIRIRPDIKIKSSSVI
CCCCECCCHHHHHHHHCCCCCEEECCCCEEEEEEECCCCEEEEEEEECCCCEEEEEEEECCEEEEEEEEECCCECEEEEECCEEEEEEEEECCCCEEEEEEECCCCCEEEEEEEC
CHHHCCCCHHHHHHHHCCCCEEECCCCCEEEEEECCCCCEEEEEEECHHCCEEEEEEEECCEEEEEEEECCCEEEEEEECCCCCEEEEEEECCCCCEEEEEEECCCEEEEEEECC
> 6cp9_1
TAQTIANSVVDAKKFDYLFGKATGNSHTLDRTNQLALEXKRLGVADDINGHAVLAEHFTQATKDSNNIVKKYTDQYGSFEIRESFFIGPSGKATVFESTFEVXKDGSHRFITTIPKNG
CHHHHHCCCCCHHHHHHHCCCCCCECCECCCHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHCCCHHHEEEEEECCCCEEEEEEEEEECCCCCEEEEEEEEEECCCCCEEEEEEEEECC
CHHHHCHEEECCCCCCEEECCCCCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCEEEEEEEEECCCCCEEEEEEEEECCCCCCCEEEEEEECCC
> 6d34_1
PASPNIEAILASYAGFRDRDIEGILSGMHPDVEWVHPEGMGKYGLGGTKLGHAGIKEFLAHVPTVLGGMRLAPREFIEQGDRVVVFGTREVTSLRGTTATLDFVHSWTMRDGKATRMEDIFDTVAFHELIES
CCCHHHHHHHHHHHHHHCCCHHHHHCCEEEEEEEEECHHHHHHCCCEEEEHHHHHHHHHHHHHHCEEEEEEEEEEEEEECCEEEEEEEEEEEECCCCEEEEEEEEEEEEECCEEEEEEEECCHHHHHHHHCC
CCCHHHHHHHHHHHHHHCCCHHHHHHHCCCCEEEEECCCCCCCCCCCEEECHHHHHHHHHHHHHHCCCEEEEEEEEEECCCEEEEEEEEEEEECCCCEECCEEEEEEEECCCEEEEEEEHCCHHHHHHHHCC
> 6rap_1
AITADDIAVQYPIPTYRFIVTLGDEQVPFTSASGLDINFDTIEYRDGTGNWFKMPGQRQAPNITLSKGVFPGKNAMYEWINAIQLNQVEKKDIMISLTNEAGTEVLVSWNVSNAFPTSLTSPSFDATSNEIAVQQITLMADRVTIQTA
CCCCCCHHHHCCCCCCCEEEEECCEECCCCEEECCCCCCCEEEEECCCCCEEEEECCCCCCEEEEEEECCCCCCCCHHHHCCCECCECCCEEEEEEEECCCCCCEEEEEEEEEEEEEEEECCCCCCCCCCCCEEEEEEECCCEEEEEC
CCCHHHCCCCCCCCCEEEEEEECCCEEEEEEEECCCEEEEEEEHHCCCCCCCECCCCCCCCCEEEEECCECCCHHHHHHHHHHHCCCCEECCEEEEEECCCCCCEEEEEEEECCCEEEEECCCCCCCCCEEEEEEEEEEHCCEEEECC
> 6d7y_2
XKELFEVIFEGVNTSRLFFLLKEIESKSDRIFDFNFSEDFFSSNVNVFSELLIDSFLGFNGDLYFGVSXEGFSVKDGLKLPVVLLRVLKYEGGVDVGLCFYXNDFNSAGKVXLEFQKYXNGISADFGFENFYGGLEPASDQETRFFTNNRLGPLL
CCEEEEEEEECCCCCCHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCCCHHHHHHHHHCCCCEEEEEEEEEEEEECCEEEEEEEEEEEEECCEEEEEEEEEECCHHHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCCCCHHHEEEECCEECCCC
CCHHEEEEECCCCHHHHHHHHHHHHCCCCCCCEEEECCCEECCCCCHHHHHHHHHHHCCCCCEEEEEEECCCCCCCCEECCEEEEEEEEECCEEEEEEECCCCHHHHHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCCEEECCCCCCCCC
> 6cp8_3
VNSTAKDIEGLESYLANGYVEANSFNDPEDDALECLSNLLVKDSRGGLSFCKKILNSNNIDGVFIKGSALNFLLLSEQWSYAFEYLTSNADNITLAELEKALFYFYCAKNETDPYPVPEGLFKKLXKRYEELKNDPDAKFYHLHETYDDFSKAYPLNN
CCCHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHCCCCCHHHHCHHHHHHHHHHHCCCCC
CCCHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCHHHHHHHHHHHHEECCCCCCCCCCHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHCCCCC
> 6cp8_1
NSFEVSSLPDANGKNHITAVKGDAKIPVDKIELYXRGKASGDLDSLQAEYNSLKDARISSQKEFAKDPNNAKRXEVLEKQIHNIERSQDXARVLEQAGIVNTASNNSXIXDKLLDSAQGATSANRKTSVVVSGPNGNVRIYATWTILPDGTKRLSTVTGTFK
CCCEEEEEECCCCCEEEEEEECCEEEECHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHCCCCECCECEEEEEEEECCEEEEEEEEEEECCCCCEEEEEEECCCC
CCCCEEECCCCCCCEEEEEEECCEEECECEEEEECCCCCCCCHHHHHHHHHHHHHHHHCCHHHHCCCCCCHHHHHHHCCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEEEEEECCCCCEEEEEEEEECCCCCCEEEEEHCCCC
> 6e4b_1
AMRLWLIRHGETQANIDGLYSGHAPTPLTARGIEQAQNLHTLLHGVSFDLVLCSELERAQHTARLVLSDRQLPVQIIPELNEMFFGDWEMRHHRDLMQEDAENYSAWCNDWQHAIPTNGEGFQAFSQRVERFIARLSEFQHYQNILVVSHQGVLSLLIARLIGMPAEAMWHFRVDQGCWSAIDINQKFATLRVLNSRAIGVEN
CEEEEEEECCCEHHHHHCECCCCCCCCECHHHHHHHHHHHHHCCCCCCCEEEECCCHHHHHHHHHHCCCCCCCEEECHHHCCCCCHHHCCCEHHHHHHHCHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHHHHHHCCCCHHHHHHECCCCCCEEEEEEECCEEEEEEEEECCCCCCC
CCEEEEEECCCCHHHHCCCEECECCCCCCHHHHHHHHHHHHHHHCCCCCEEEECHHHHHHHHHHHHHHCCCCCEEECCCHEECCCCCECCCCHHHHHHHCHHHHHHHHHCHHHEECCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCHHHHHHHHHHHCCCHHHHECEEECCCEEEEEEECCCCEEEEEECCCCCECCC
> 6rbk_1
SLLERGLSKLTLNAWKDREGKIPAGSMSAMYNPETIQLDYQTRFDTEDTINTASQSNRYVISEPVGLNLTLLFDSQMPGNTTPIETQLAMLKSLCAVDAATGSPYFLRITWGKMRWENKGWFAGRARDLSVTYTLFDRDATPLRATVQLSLVADESFVIQQSLKTQSAPDRALVSVPDLASLPLLALSAGGVLASSVDYLSLAWDNDLDNLDDFQTGDFLRAT
CCCCCCCCCCEEEEECCCCCCCEEEEEECCCCCCCEEEEEEEEEEECCCCCCCCCCEEEEEEEEEEEEEEECCECCCCCCCCCHHHHHHHHHHHCCCCCCCCCCCCCEEECCCCCCCCCCEECCCEEECCCCCCCECCCCCECECCEEEEECCCCCCCHHHHHCCCCCCCCCCEECCCCCCHHHHHHHHHHHCCCCCCHHHHHHHCCCCCCCCCCCCCEECCC
CCCCCHHHHEEEEECCCCCCCCCCCEEEEEECCCEEEEEEEEEECCCCCCCCCCCCCCECCCCCCEEEEEEEEECCCCCCCCCHHHHHHHHHHHEECCCCCCCCCEEEEEECCCCCCCCCEEEEEEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCHHHHCCCCCCCCCCCEEEEECCCCCCHHHHHHHHHHCCHHHHHHHHHHCCCCCCCCCCCCCEEEEC
> 6f45_1
AVQGPWVGSSYVAETGQNWASLAANELRVTERPFWISSFIGRSKEEIWEWTGENHSFNKDWLIGELRNRGGTPVVINIRAHQVSYTPGAPLFEFPGDLPNAYITLNIYADIYGRGGTGGVAYLGGNPGGDCIHNWIGNRLRINNQGWICGGGGGGGGFRVGHTEAGGGGGRPLGAGGVSSLNLNGDNATLGAPGRGYQLGNDYAGNGGDVGNPGSASSAEMGGGAAGRAVVGTSPQWINVGNIAGSWL
CCCCCCHHHHHHHHHCCCEHHHHHHHCCCCCCCEEHHHHCCCCCCEEEEECCCEEEECHHHHHHHHHHCCCCCEEEEECCCEEECCCCCCCEEECCCCCCCCEEEEECCEEECCCCCCCECCCCCCCCCCCEEECCHHHEEEEECCEEECCCCCCCCEEECCEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEECCEECCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCEEEECCEEECCCC
CCCCCCCCCCHHHHCCCCCHHHHCEEEEEEECCCCHHHHCCCCCCEEEEEECCCCCCCCCCECCCCCCCCCCCCECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHHCC
> 6mxv_1
IXQHSSGFLKLVDDAKSRIQECSVDDIQKXNETQTLDGLLIDTREESEVANGYIPNAIHLSKGIIESAIESAVPNKNQKXYFYCGGGFRSALVADKLREXGYKNVISVDGGWRAWNAKGYPTVSPNQFRPNEFLKLVNNAKTQIKECSTTELYNKINSQELDGIVFDVREDSEFNRFHIQGATHLSKGQIEVKIENLVPNKQQKIYLYCGSGFRSALAAESLQHXGYTNVVSIAGGIKDWLANNYPVSQN
CCCCCHHHHHHHHHHHCCCEEECHHHHHHHHHHCCCCCEEEECCCHHHHCCCECCCEEECCCCCHHHHHHHHCCCCCCCEEEECCCCCHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECHHHCCCHHHHHHHHHHHHHCEEECHHHHHHHHHCCCCCCEEEECCCHHHHHHCECCCCEECCCCCHHHHHHHHCCCCCCCEEEECCCCHHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCCECC
CCCCCCHHHHHHHHHHHCCCCCCHHHHHHHHHCCCCCEEEEEECCHHHHHHCCCCCCEECCCCCHHHHHHHCCCCCCCEEEEEECCCCHHHHHHHHHHHCCCCCEEEECCHHHHHHHCCCCEECCCCCCCHHHHHHHHHHHCCCCCCCHHHHHHHHHCCCCCEEEEECCCHHHHHHCCCCCCEECCHHHHHHHHHHCCCCCCCCEEEEECCCCHHHHHHHHHHHCCCCCEEEHHCCHHHHHHCCCCECCC
> 5z82_1
SIGLAHNVTILGSGETTVVLGHGYGTDQSVWKLLVPYLVDDYKVLLYDHMGAGTTNPDYFDFDRYSSLEGYSYDLIAILEEFQVSKCIYVGHSMSSMAAAVASIFRPDLFHKLVMISPTPRLINTEEYYGGFEQKVMDETLRSLDENFKSLSLGTAPLLLACDLESAAMQEYCRTLFNMRPDIACCITRMICGLDLRPYLGHVTVPCHIIQSSNDIMVPVAVGEYLRKNLGGPSVVEVMPTEGHLPHLSMPEVTIPVVLRHIRQDITD
CHHHHCCCEEEECCCCEEEEECCCCCCHHHHCCCHHHCCCCCEEEEECCCCCCCCCHHHCCCCHHHCCHHHHHHHHHHHHHCCCCCEEEEEECHHHHHHHHHHHHCHHHEEEEEEECCCCCCECCCCCCCCECHHHHHHHCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHCCHHHHHHHHHHHHCCCCHHHHHHCCCCEEEEEEEECCCCCHHHHHHHHHHCCCCEEEEEEEEECCCHHHHCHHHHHHHHHHHHHCCCCC
CHHHECCEEEECCCCEEEEEECCCCCCCHHHHHHHHHHHCCCEEEEEEECCCCCCCHHCCCHHHCCCHHHHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHHCCHHHCEEEEEECCCCECCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHCCCHHHHHHHHHHHHCCCCHHHHCCCCCCEEEEECCCCEECCHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCHHHHHHHHHHHHHHCCCC
> 6rap_5
SNYQTLVDVNNAMNKMLRAYVNEAVAIRFDLPDTQADAAISVFLYDIHEDLQLRTAESRGFNAGAGRLLPGWVNVKCNYLITYWESPDSQPDNQAIQVMSQVLAALINNRQLADIGAYTQVMPPKENLNSLGNFWQSLGNRPRLSLNYCVTVPISLSDKGEEMTPVKSLSTTVEPKAPLSPLVITDALREQLRVALDACLAMTHVNLDSSPVANSDGSAAEIRVSLRVYGMTPTEYLAPMNTVFNEWEKSEAAAVTPDGYRVYINAVDKTDLTGI
CCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCEEECCCCECHHHCCCCCCCCCCCCCCCCCCCEEEECCEEECCCCCCCCCCCCCCCCCHHHHHHHHHHCCCCCCCCCEEECCCCCCCHHHHHHHHHHCCCCCCCCEEEEEEEEECCCCCCCCCCECCCEEEEEEECCCCCHHHHHHHHHHHHHHHCCHHHHHHHECCEEEEECCCCCCCCCCEEEEECEECCCCCCHHHHHHHHHHHHCCCCCCCCCCCCCCEECCCCCCECCCC
CCCHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCCCCEEEEEEEEECCCHHHHCCCCCCCCCCCCCECCCCEEEEEEEEEEEECCCCCCCCHHHHHHHHHHHHHHHCCCHHCCCCCCEEEECCCCCCCHHHHHHHHCCCCCCEEEEEEEEEEECCCCCCCCCCCEEEEEEEECCCCCCCCHHHHHHHHHHHHHHHHHHHHHEEECCCCCCCCCCCCCCCCCEEEEEEECCCCHHHHHHHHHHHHHHHCCCEEEEEECCCEEEEEECCHHHHECC
> 6d2v_1
GLVPRGSHMEGKKILVTGGTGQVARPVAEALAERNEVWCLGRFGTPGVEKELNDRGITTFHWDMDDPGAAAYEGLPDDFTHVLHSAVRRGEDGDVNAAVEVNSVACGRLMTHCRGAEAFLFVSTGALYKRQTLDHAYTEDDPVDGVADWLPAYPVGKIAAEGAVRAFAQVLNLPTTIARLNIAYGPGGYGGVPMLYFKRMLAGEPIPVPKEGQNWCSLLHTDDLVAHVPRLWEAAATPATLVNWGGDEAVGITDCVRYLEELTGVRARLVPSEVTRETYRFDPTRRREITGPCRVPWREGVRRTLQALHPEHLPS
CCCCHHHHCCCCEEEEECCCCCCHHHHHHHHHCCCEEEEEECCCCCCHHHHHHHCCCEEEECCCCCCCHHHHCCCCCCCCEEEECCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEEEHHHECCCCCCCEECCCCCECCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCEEEEEECCEECCCCCCEHHHHHHHHHHHCCCEEEECCCCCEECCEEHHHHHHHHHHHHHCCECCCEEEEECCCCCEEHHHHHHHHHHHHCCCCCEEEECCCCCCCEECCHHHHHHHCCCCCCHHHHHHHHHHHHCHHHCCC
CCCCCCCCCCCCEEEEECCCCHHHHHHHHHHHHCCCEEEECCCCCHHHHHHHHCCCCCEEEEEECCHHHHHHHHCCCCCCEEEEECHCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCEEEEEECHHHCCCCCCCCCCCECCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHCCCCEEEEEEEECCCCCCCCHHHHHHHHHHCCCCEEECCCCCCCCCCEEEHHHHHHHHHHHHCCCCCCEEEEECCCCCHHHHHHHHHHHHHCCCCCCECCCCCCCCCCCCCHHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCC
> 6q64_1
LIPLIEAQTEEDLTPTXREYFAQIREYRKTPHVKGFGWFGNWTGKGNNAQNYLKXLPDSVDFVSLWGTRGYLSDEQKADLKFFQEVKGGKALLCWIIQDLGDQLTPKGLNATQYWVEEKGQGNFIEGVKAYANAICDSIEKYNLDGFDIDYQPGYGHSGTLANYQTISPSGNNKXQVFIETLSARLRPAGRXLVXDGQPDLLSTETSKLVDHYIYQAYWESSTSSVIYKINKPNLDDWERKTIITVEFEQGWKTGGITYYTSVRPELNSXEGNQILDYATLDLPSGKRIGGIGTYHXEYDYPNDPPYKWLRKALYFGNQVYPGKFD
CCCHHHCCCHHHCCHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCCCHHHCHHHCCCCCCEEECCCCCCCCCHHHHHHHHHHHHCCCCEEEEEEEECCCCCCCCCCCCCHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCCCCCCCCCECCCCCHHHHHHHHHHHHHHCCCCCEEEEEECHHHECHHHHCCCCCEEEECCCCCEHHHHHHHHCCCCCCCHHHHEEEEEEHHHHCCCCCCCCCECCCHHHHHCCCHHHHHHHCCECCCCCECCEEEEECHHHHCCCCCCCHHHHHHHHHHHHHCCCCCC
CCCCCCCCCHHCCCHHHHHHHHHHHHHHCCCCEEEEEEECCCCCCCCCCCCHHHHCCCCCEEEEECCCCCCCCHHHHHHHHHHHCCCCCEEEEEEEHHHHCCCCCCCCCCCHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEECCCCCCCCCCCCCCCCHCHHHHHHHHHHHHHHHHHCCCCCCEEEECCCCCCCCHHHHHHHCEEEEECCCCCCCCCCCECCCCCCCCCCCCCEEEEHCCHHHHHCCCCCCCCCCCCCCCCCCCHHHHHHHECCCCCCCCEEEEEEEEHCCCCCCCCCCHHHHHHHHHHCCCCCCCCC
> 6n91_1
MITSSLPLTDLHRHLDGNIRTQTILELGQKFGVKLPANTLQTLTPYVQIVEAEPSLVAFLSKLDWGVAVLGDLDACRRVAYENVEDALNARIDYAELRFSPYYMAMKHSLPVTGVVEAVVDGVRAGVRDFGIQANLIGIMSRTFGTDACQQELDAILSQKNHIVAVDLAGDELGQPGDRFIQHFKQVRDAGLHVTVHAGEAAGPESMWQAIRDLGATRIGHGVKAIHDPKLMDYLAQHRIGIESCLTSNLQTSTVDSLATHPLKRFLEHGILACINTDDPAVEGIELPYEYEVAAPQAGLSQEQIRQAQLNGLELAFLSDSEKKALLAKAALRG
CCCCCCCCEEEEEEHHHCCCHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHCCEEEEEEEEEHHHCHHHHHHHHHHHHCCHHHCCEEEEECCCCCCCHHHHHHHHHHHHHCCCEEEEEECCCCCHHHHHHHHHHCCCCEEEECHHHHHCHHHHHHHHHHCCEEEECHHHHHHCCCCCCHHHCCHHHHHHCCCCEEECCECHHHHCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHCCC
CCCCCCCEEEEEECCCCCCCHHHHHHHHHHCCCCCCCCCHHHHHHHEEEECCCCCHHHHHHHHCCCHHHCCCHHHHHHHHHHHHHHHHHHCCEEEEEEECHHHHHHCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCHHHHHHHHHHHHHHCCCEEEEEECCCCCCCCCHHHHHHHHHHHHCCCCEEEECCCCCCCHHHHHHHHHCCCEEECCCEEHHHCHHHHHHHHHCCCEEEECCCCHCCCCCCCCHHHCCHHHHHHCCCEEEEECCCCCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHCCC
> 6cvz_1
KHKYHFQKTFTVSQAGNCRIMAYCDALSCLVISQPSPQASFLPGFGVKMLSTANMKSQYIPMHGKQIRGLAFSSYLRGLLLSASLDNTIKLTSLETNTVVQTYNAGRPVWSCCWCLDEANYIYAGLANGSILVYDVRNTSSHVQELVAQKARCPLVSLSYMPRAASAAFPYGGVLAGTLEDASFWEQKMDFSHWPHVLPLEPGGAIDFQTENSSRHCLVTYRPDKNHTTIRSVLMEMSYRLDDTGNPICSCQPVHTFFGGPTAKLTKNAIFQSPENDGNILVCTGANSALLWDAASGSLLQDLQTDQPVLDICPFEVNRNSYLATLTEKMVHIYKWE
CCCEEEEEEEECCCCCCCCCEEEECCCCEEEEEEECCCCCCCCCEEEEEEECCCCCCEEEEEECCCEEEEEECCCCCCEEEEEECCCEEEEEECCCCEEEEEEECCCCEEEEEECCCCCCEEEEEECCCCEEEEECCCCCCCCEEECCCCCCCCEEEEEEECCCCCCCCCCCEEEEEECCEEEEEEECCCCCEEEEECCCCEEEEEEEEEECCCCEEEEEEEECCCCCCCEEEEEEEEEEECCCCCEEEEEEEEEEEECCCCCCCCECEEEECCCCCCCEEEEECCCEEEEEECCCCCEEEEEECCCCCCEEEEEEECCEEEEEEECCCEEEEEEEC
CCCEEEEEEEEECCCCCCEEEEECCCCCEEEEEECCCCCCCCCCCEEEEECCCCCCEEECCCCCCCEEEEEECCCCCCEEEEEECCCCEEEEEECCCEEEEEEECCCCEEEEEECCCCCCEEEEEECCCEEEEEECCCCCCCCHEECCCCCCCCEEEEEECCCCCCCCCCCCEEEEEEECCEEEEECCCCCCCCCCCCCCCCCCEEEEEECCCCCEEEEEEECCCCCCCCHEEEEECCCCCCCCCCCEEEEEEEEEECCCCCCEECCCEEEECCCCCCEEEEEECCCCEEEEECCCCCEEEECCCCCCEEEEEEEECCCCCEEEEEECCEEEEEECC
> 6ek4_1
VVYPEINVKTLSQAVKNIWRLSHQQKSGIEIIQEKTLRISLYSRDLDEAARASVPQLQTVLRQLPPQDYFLTLTEIDTELEDPELDDETRNTLLEARSEHIRNLKKDVKGVIRSLRKEANLMASRIADVSNVVILERLESSLKEEQERKAEIQADIAQQEKNKAKLVVDRNKIIESQDVIRQYNLADMFKDYIPNISDLDKLDLANPKKELIKQAIKQGVEIAKKILGNISKGLKYIELADARAKLDERINQINKDCDDLKIQLKGVEQRIAGIEDVHQIDKERTTLLLQAAKLEQAWNIFAKQLQNTIDGKIDQQDLTKIIHKQLDFLDDLALQYHSMLLS
CCCCCCCHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHCCCECCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCEHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHCCCCC
CCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHC
> 6rap_3
TTTYPGVYLSEDAVSSFSVNSAATAVPLFAYDSENTNTINKPIQVFRNWAEFTVEYPTPLEDAFYTSLSLWFMHGGGKCYLVNEANIADAVAQYDDITLIVAAGTDTTTYTAFTTVVGQGYRIFGLFDGPKEKIAGTAKPDEVMEEYPTSPFGAVFYPWGTLASGAAVPPSAIAAASITQTDRTRGVWKAPANQAVNGVTPAFAVSDDFQGKYNQGKALNMIRTFSGQGTVVWGARTLEDSDNWRYIPVRRLFNAVERDIQKSLNKLVFEPNSQPTWQRVKAAVDSYLHSLWQQGALAGNTPADAWFVQVGKDLTMTQEEINQGKMIIKIGLAAVRPAEFIILQFSQDI
CCCCCCCCCCCCCCCCCCCCCCCCCCCEEEECCCCCCCCCCCEEEECCHHHHHHHCCCCCCCCCHHHHHHHHCCCCCCEEEEECCCHHHHHHHHCCCCCEEECCCCCCHHHHHHHHHHCCCCCCEECCCCCCCCCCCCCHHHCCCCCCCCCCEECCCCCECCCCCCCECHHHHCCHHHHHHHCCCCCCCCCCCCECCCCCECCCCCHHHHCCCCCCCCCCEEECCCCCCCEEECCCCCCCCCCCCCHHHHHHHHHHHHHCCCCCHHHCCCECCHHHHHHHHHHHHHHHHHHHHCCCECCCCHHHHEEEECCCCCCCCHHHHCCCCEEEEEEECECCEECEEEEEEECCC
CCCCCCEEEEECCCCCCCHHCCCCCCEEEEECCCCCCCCCCCCEEECCHHHHHHHHCCCCCHHHHHHHHHHHHCCCCCCEEEEECCCHHHHHHCCCCEEEECCCCCHHHHHHHHHHHHHHCCEEEEEECCCCCCCCCCCHHHHCCCCCCCCCHEEECCHHCCCCEEECCCCHHHHHHHHHCCCCCCEEEECCCHHHCCCCCHHECCHHHHHHHCCCCCEEEEEECCCCCEEEEEEEECCCCCCCCEEEHHHHHHHHHHHHHHHCCEEEECCCCHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHEEEECCCCCCCCHHHHHCCCEEEEEEEECCCCCEEEEEEEECCC
> 6cci_1
VELPPEEADLFTGEWVFDNETHPLYKEDQBEFLTAQVTCMRNGRRDSLYQNWRWQPRDASLPKFKAKLLLEKLRNKRMMFVGDSLNRNQWESMVBLVQSVVPPGRKSLNKTGSLSVFRVEDYNATVEFYWAPFLVESNSDDPNMHSILNRIIMPESIEKHGVNWKGVDFLVFNTYIWWMNTFAMKVLRGSFDKGDTEYEEIERPVAYRRVMRTWGDWVERNIDPLRTTVFFASMSPLHIKSLDWENPDGIKDALETTPILNMSMPFSVGTDYRLFSVAENVTHSLNVPVYFLNITKLSEYRKDAHTSVHTIRQGKMLTPEQQADPNTYADDIHWCLPGLPDTWNEFLYTRIISR
CCCCCCCCCCCCEEEEECCCCCCCCCHHHCCCCCCCCCCCCCCCCCCHHHHEEEEECCCCCCCCCHHHHHHHCCCCEEEEEECHHHHHHHHHHHHHHHCCCCCCCEEEEEECCEEEEEECCCCEEEEEEECCCCCCECCCCCCCCCCCCCEECCCCCHHHHHHHCCCCEEEECCCHHHCCCCEEEEECCCHHHCCCCEEEEEHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCCCEEEECCHHHHHCCCCCEEECCCEECCEECCHHHHCCHHHHCEEEEECCCCHHHHHHHHHHHHHHCC
CCCCCCCCCCCCEEEEECCCCCCCCCCCCCCCCCHHHHHHHCCCCCCHCCEEEECCCCCCCCCCCHHHHHHHHCCCEEEEEECCCCCCHHHHHHHHHHCCCCCCCCEEECCCCCEEEEEHCHCCEHHHHHHHCEEEECCCCCCCCCCCCCEEEHHHHHHHHHHCCCCCEEEEECHHHHHCCCEEEEEECCCCCCCCCEEEECHHHHHHHHHHHHHHHHHHCCCCCCCEEEEEECCCCCCHHHHCCCCCCCCEHCCCCHCCCCCCCCCCCCCHHHHHHHHHHHHCCCCEEEEEEECCCCHHHHCCCCCCCCCCCCCCCCHHHHCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHCC
> 6cl6_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKEADGELPGGVNLDSMVTSGWWSQSFTAQAASGANYPIVRAGLLHVYAASSNFIYQTYQAYDGESFYFRCRHSNTWFPWRRMWHGGDFNPSDYLLKSGFYWNALPGKPATFPPSAHNHDVGQLTSGILPLARGGVGSNTAAGARSTIGAGVPATASLGASGWWRDNDTGLIRQWGQVTCPADADASITFPIPFPTLCLGGYANQTSAFHPGTDASTGFRGATTTTAVIRNGYFAQAVLSWEAFGR
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHHCECCCCCCCEEEEEEEEECCEEEEEEEECCCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCHHHCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEECCCEEEEEECCCCCCCCEEEEEEEECCCCCCCCCCCEEEECCCCCEEEEEECCCCCEEEEEEEEEC
CCHCCCCHHHHHHHHHCCCCCCCCCCCCCCCCECCCCCCCCCCCCCCCCEEEECCCCCCCCCCCCEEEEECCCCCCCCCEEEEEECCCCHEEEHHECCCCCCCCCCCCHHHHCCCCCCCCCCCCCCCCCCCCCHECCCCEEEECCCCCCCCCCCCCCCCCCEEEEECCCCCCEEEEEEECCCCEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCHCCHHHCCCCCCCCCCCCEECECCCCCCEEEEEEEEEECCCCCEEEEECCCCCCCEEEEEEEECCCCCCCCCCCEEEECCCCCEEEEECCCCCCCEEEEEEECC
> 6cl5_1
ALAATDIPGLDASKLVSGVLAEQRLPVFARGLATAVSNSSDPNTATVPLMLTNHANGPVAGRYFYIQSMFYPDQNGNASQIATSYNATSEMYVRVSYAANPSIREWLPWQRCDIGGSFTKTTDGSIGNGVNINSFVNSGWWLQSTSEWAAGGANYPVGLAGLLIVYRAHADHIYQTYVTLNGSTYSRCCYAGSWRPWRQNWDDGNFDPASYLPKAGFTWAALPGKPATFPPSGHNHDTSQITSGILPLARGGLGANTAAGARNNIGAGVPATASRALNGWWKDNDTGLIVQWMQVNVGDHPGGIIDRTLTFPIAFPSACLHVVPTVKEVGRPATSASTVTVADVSVSNTGCVIVSSEYYGLAQNYGIRVMAIGY
CCCHHHCCCEEHHHEEECCCCHHHCCHHHHCECCCCCCCCCHHHCCCCEEEECCCCCCCCCCCEEEEEEEECCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCECCCEECCHHHECECCCCEECCCCCEHHHCCCCEEEEECCHHHHHCCECCCCCCCEEEEEEEEECCEEEEEEEECCCCEEEEEEECCEECCCEEECECCCCCHHHECEHHHCCCCCCCCCCCCECECCCCEEHHHEEECCCCHHHCCCCCCCHHHHHHHHCCCCCCEEECCCCEEEEECCCCEEEEEEEEEEECCCCCCEEEEEECCCCCCCCEEEEEEEEEEEECCCCCHHHEEEEEEEECCCEEEEEEEECCCCCCEEEEEEEEEEC
CCHCCCCHHHHHHHHHCCCHHHCCCCCCCCCCCCCCCCCCCCCCCCCCEEEEECCCCCCCCCEEEECCCECCCCCCCCCEEEEECCCCCHEEEEEEECCCCCCCCCCCHHHECCCCCCCCCCCCCCCCCCCCCCEECCEEEEECCCCCCCCCCCCCCCCCCEEEEEECCCCEEEEEEEECCCCEEEEEECCCCCCCHEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEECCCCCEEEEEEEEEECCCCCCCCCEEEEEECCCCCCEEEEEEEEECCCCCCCCCCEEEEEEEEECCCCEEEEECCCCCCCCCCEEEEEEECC
> 6hrh_1
LYFQSMFSYDQFFRDKIMEKKQDHTYRVFKTVNRWADAYPFAQHFSSKDVSVWCSNDYLGMSRHPQVLQATQETLQRHGVGAGGTRNISGTSKFHVELEQELAELHQKDSALLFSSCFVANDSTLFTLAKILPGCEIYSDAGNHASMIQGIRNSGAAKFVFRHNDPDHLKKLLEKSNPKIPKIVAFETVHSMDGAICPLEELCDVSHQYGALTFVDEVHAVGLYGSRGAGIGERDGIMHKIDIISGTLGKAFGCVGGYIASTRDLVDMVRSYAAGFIFTTSLPPMVLSGALESVRLLKGEEGQALRRAHQRNVKHMRQLLMDRGLPVIPCPSHIIPIRVGNAALNSKLCDLLLSKHGIYVQAINYPTVPRGEELLRLAPSPHHSPQMMEDFVEKLLLAWTAVGLPLQCRRPVHFELMSEWERSYFGNM
CCCCCCCCHHHHHHHHHHHHHHHCCCCCCCEEEECCCCCCEEEEECCEEEEECCCCCCCCHHHCHHHHHHHHHHHHHHCCCCCCCCCCCCECHHHHHHHHHHHHHCCCCEEEEECCHHHHHHHHHHHHHHHCCCCEEEEECCCCHHHHHHHHHHCCEEEEECCCCHHHHHHHHHCCCCCCCEEEEEECECCCCCCECCHHHHHHHHHHCCEEEEEECCCCCCCCCCCCCCHHHHCCCHHHCCEEEEECCCCCCCCCEEEEECHHHHHHHHHHCHHHHCCCCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHCCCCECCCCCCCEEEECCCHHHHHHHHHHHHHCCCEECCEECCCCCCCCCCEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHCCCC
CCCCCCCCHHHHHHHHHHHHHHCCCEEEEHHHHHCCCCCCCCECCCCCCEEEEEEHCHCHHCCCHHHHHHHHHHHHHCCCCCCCEECCCCCCHHHHHHHHHHHHHHCCCHHEHHHHHHHHHHHHHHHHHHHCCCCEEEECCCCHHHHHHHHHCCCCEEEEECCCCHHHHHHHHHCCCCCCCEEEEEECEECCCCCECCHHHHHHHHHHHCCEEEEECEEECCCCCCCCCCEEHHCCCCCCEEEEEECHHHHCCECCCEEHHCHHHHHEEHCCCCCCHHHHHCCHHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHCCCCEECCCCCEEEEEECCHHHHHHHHHHHHHHCCEEEEEEECCCCCCCHEEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCCHHCCCCHHHHHHHHHHHCCCCCC
> 6m9t_1
DCGSVSVAFPITMLLTGFVGNALAMLLVSRSYRRRESKRKKSFLLCIGWLALTDLVGQLLTTPVVIVVYLSKQRWEHIDPSGRLATFFGLTMTVFGLSSLFIASAMAVERALAIRAPHWYASHMKTRATRAVLLGVWLAVLAFALLPVLGVGQYTVQWPGTWAFISTNWGNLFFASAFAFLGLLALTVTFSCNLATIKALVSRGSNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLNAAKSELDKAIGRNTNGVITKDEAEKLFNQDVDATVRGILRNAKLKPVYDSLDAVRRAALINMVFQMGETGVAGFTNSLRMLQQKRWDEAAVNLAKSRWYNQTPNRAKRVITTFRTGTWDAYGSWGRITTETAIQLMAIMCVLSVCWSPLLIMMLKMIFNEKQKECNFFLIAVRLASLNQILDPWVYLLLRKILGRPLEVL
CCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHCCCCHHHHHHHHHHHHHHHHHHCHHHHCCCCEEEECCCCEEEECCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHCCEEEEEECCCCCEEEECCEEEECCCCHHHHHHHHHHHHCCCCCCECCHHHHHHHHHHHHHHHHHHHHHCCCHHHHHHHCCHHHHHHHHHHHHHHCHHHHHCCHHHHHHHHCCCHHHHHHHHHCCHHHHHCHHHHHHHHHHHHHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHC
CCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCHHEEHHHHHHHHHHHHHHCCCHHEEHHHHCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHECCCCCCCCCHCCCHHHHHHHHHHHHHHHHHHHCCHCCCCCEEEECCCEEEEECCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHHHCCCCHHHCHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCCCHHCEEEECCCCCCCCCHHHHHHHHHCCCCCCCCCCCEEHECCCCCCCHHHEHEHCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHCCCCCCHHHHEEEEEHHHHHCCHHHHHHHHHHHHHHHHHHCC
> 5w6l_1
NAQELKERAKVFAKPIGASYQGILDQLDLVHQAKGRDQIAASFELNKKINDYIAEHPTSGRNQALTQLKEQVTSALFIGKXQVAQAGIDAIAQTRPELAARIFXVAIEEANGKHVGLTDXXVRWANEDPYLAPKHGYKGETPSDLGFDAKYHVDLGEHYADFKQWLETSQSNGLLSKATLDESTKTVHLGYSYQELQDLTGAESVQXAFYFLKEAAKKADPISGDSAEXILLKKFADQSYLSQLDSDRXDQIEGIYRSSHETDIDAWDRRYSGTGYDELTNKLASATGVDEQLAVLLDDRKGLLIGEVHGSDVNGLRFVNEQXDALKKQGVTVIGLEHLRSDLAQPLIDRYLATGVXSSELSAXLKTKHLDVTLFENARANGXRIVALDANSSARPNVQGTEHGLXYRAGAANNIAVEVLQNLPDGEKFVAIYGKAHLQSHKGIEGFVPGITHRLDLPALKVSDSNQFTVEQDDVSLRVVYDDVANKPKITFKG
CCCCHHHHHHCCCCCCCHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHCCHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHCHHHCCCCCCCCCCCCCCCCCCCEEEECCCCHHHHHHHHHHHHHCCCCCCEEEECCCCEEEECCCHHHHHHCCCCHHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHECHHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHHHCCCCCCCHHHHHHHCCCHHHHHHHHHCCCCEEEECCCCCCCCHHHHHHHHCHHHHHHHCEEEEEECCCCCCCHHHHHHHHHHHCCCCHHHHHHHHHCCCCHHHHHHHHHCCCEEEECCCHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHCCCCCCEEEEECCHHHHCCEEECCCEECCHHHHHCCCEEEECCCCCEEECCCCHHHCCECCCCCCCCCCCCCC
CCHHHHHHHCEEECCCCCCHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCCCCCCCCHHHCCCCCEEEEEHCHHHHHHHHHHHHHCCCCCCCCEEECCCCCEEECCCCHHHHHCCCCCCHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHCCHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCEEEEECCCCCCHHHHHHHHHHHHHHHCCCCEEEEECHCCHHHHHHHHHHHHCCCCCHHHHHHHHCCCCHHHHHHHHHHCCCEEEEECCCCHCCCCCCCCCCCHHHHHHHHHHHHHHHHHHCCCCCEEEEEECCCEEECCCCCCCCCCCHHHHCCCEEEEEEECCCCCCCCCCCCCCEECCCCCCCCCEEECC
> 6rbk_3
HITLDIAGQRSTLGIRRLRVQQLINEIPLAQLELHIPTDNHGAADNAVQHEVSRFTLGVRVGIAQDNKPLFDGYLVQKKMQLKGKEWSVRLEARHALQKLTFLPHSRVFRQQDDSTVMKGLLQSAGVKLTQSKHDQLLQFRLSDWQFIRSRLLSTNCWLLPDAASDTVVIRPLSSRTLARDSHDYTLYEINLNFDNRFTPDSLSLQGWDIAAQRLTAAQKSPAGAFRPWKPAGQDYALAFSMLPEATLQTLSNSWLNYQQMTGVQGHIVLAGTRDFAPGESITLSGFGAGLDGTAMLSGVNQQFDTQYGWRSELVIGLPASMLEPAPPVRSLHIGTVAGFTADPQHLDRIAIHLPALNLPDSLIFARLSKPWASHASGFCFYPEPGDEVVVGFIDSDPRYPMILGALHNPKNTAPFPPDEKNNRKGLIVSQADQTQALMIDTEEKTLRLMAGDNTLTLTGEGNLTMSTPNALQLQADTLGLQADSNLSIAGKQQVEITSAKINM
CEEEEECCECCCCCEEEEEEEECCCCCCEEEEEEECCCCCECHHHHCCHHHHCCCCCCCEEEEEECCEECCCEEEEEEEECCCCCCCEEEEEEECHHHHHCCCCECCCCCCCCHHHHHCCCHHHCCCEEECCCCCCCCCCCECCCCCCCCCCCCCCEECCECCCCCEEECEECCCCCECCCCCCCCEEEEEECCCCCCCCCCCEEEECCCCCCCCCCCECCCCCCCCCEEECCCCCEEEECCCCCCCCHHHHHHHHHHHHCCCCCEEEEECCCCCCCCCCCEEECCCCCCCCEEECEEEEECCEECCCCECCEEEECCCCCCCCCCCCCCCCEEECECCCCCCCCCCCCEEECCCCCCCCCCCEEEEECCCCCCCCCCCCCCCCCCCCEEEEEHHHCCCCEEEEECCCCCCCCCCCCCCCCCCECCCCCECCCCECCEECECCCCCCEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CEEECCCCCCCCEEEEEEEEEEHECCCCEEEEEEEECCCCCCCHCCCCHECCCCCEEEEEEEECCCCCEEEEEEEEEEEEEECCCCEEEEEEEEHHHHHHHCCCCCEEHCCCCHHHHHHHHHHHCCCCCCCCCECEEEECCCCHHHHHHHHHHHCCEEEEEECCCEEEEECCCCCCCCEEECCCCEEEEEEEEEHHHHHHHHEEEEEECCCCCEEEEEECCCCCCCCCCCCCHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHEEEEEEEEECCCCCCCCCEEEEECCCCCCCCEEEEEEEEEEEECCCCEEEEEEEEECCCCCCCCCCCCCEEEEEEEECCCCCCCCCEEEEEECCCCCCCCEEEEEEECCCCCCCCCEEEECCCCCEEEEEECCCCCCCCEEEEECCCCCCCCCCCCCCCCEEEEEEEECCCCEEEEEECCCCCEEEEECCCCEEEEEECCCEEEECCCEEEEECCEEEEEECCCEEEEEECEEEEEECEECC
> 6n9y_1
MERFLRKYNISGDYANATRTFLAISPQWTCSHLKRNCLFNGMCAKQNFERAMIAATDAEEPAKAYRLVELAKEAMYDRETVWLQCFKSFSQPYEEDIEGKMKRCGAQLLEDYRKNGMMDEAVKQSALVNSERVRLDDSLSAMPYIYVPIKEGQIVNPTFISRYRQIAYYFYSPNLADDWIDPNLFGIRGQHNQIKREIERQVNTCPYTGYKGRVLQVMFLPIQLINFLRMDDFAKHFNRYASMAIQQYLRVGYAEEVRYVQQLFGKIPTGEFPLHHMMLMRRDFPTRDRSIVEARVRRSGDENWQSWLLPMIIVREGLDHQDRWEWLIDYMDRKHTCQLCYLKHSKQIPTCGVIDVRASELTGCSPFKTVKIEEHVGNNSVFETKLVRDEQIGRIGDHYYTTNCYTGAEALITTAIHIHRWIRGCGIWNDEGWREGIFMLGRVLLRWELTKAQRSALLRLFCFVCYGYAPRADGTIPDWNNLGSFLDTILKGPELSEDEDERAYATMFEMVRCIITLCYAEKVHFAGFAAPACESGEVINLAARMSQMRMEY
CHHHHHHHCCCHHHHHHHHHHHHHCCCCCCCCCCHHHECCCCECCCCHHHHHHHCCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCHHHHHHCCCCCCCCCCCCCCCCEECHHHHCEEECCCCECCCCEEEECCCCEEEEECCCCCCEEECCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCEEEECCCHHHHCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHCCCCCCCCCCHHHHCCCCCCCCCCCCHHHHHHCCCCCCCHHHCCEECHHHHHHCCCCCCHHHHHHHHHHCCCCCHHHHHCCCCCCEEEEECHHHHHHCCCCCEEEEEEEEECCCCCCCCCCCCCCEEEEEECCEEEEEECCCCCHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHCCC
CHHHHHHCCCCHHHHHHHHHHHHHCCCCECCECCCCCHHCCEEHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHCCCHHHHHHCCHCCCCCCEEECCCHCCCHHEECCCCCCCEEEEEEEEECCCEEEEEECCCCCCCECCCCCHHHHHHHHHHHHHHHHHCCCCCCCCCCCCEEEEEEEEHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHEEEEEECCCCCCHHHHHHHCEECCCCCCCEHHHEEHHCCCCCCHHHHHHHHHHHHHHHCCCCCHHHHHHHHCCCCECCHHHHHHCCCCCEEEEEEECCHHHCCCCCEEEEEEEEECCCCCCEEEEECCCCEEEECCCEEEEEECCCCHHHHHHHHHHHHHHHHCCCCCCCHHHHHHHHHHHEEEEEECCCCHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCEECCCCCCCCCCCCCHHEHHHHHHHHHHCC
> 6nq1_1
AARWDLCIDQAVVFIEDAIQYRSINHRVDASSMWLYRRYYSNVCQRTLSFTIFLILFLAFIETPSSLTSTADVRYRAAPWEPPAGLTESVEVLCLLVFAADLSVKGYLFGWAHFQKNLWLLGYLVVLVVSLVDWTVSLSLVAHEPLRIRRLLRPFFLLQNSSMMKKTLKCIRWSLPEMASVGLLLAIHLCLFTMFGMLLFAGLTYFQNLPESLTSLLVLLTTANNPDVMIPAYSKNRAYAIFFIVFTVIGSLFLMNLLTAIIYSQFRGYLMKSLQTSLFRRRLGTRAAFEVLSSMVGAVGVKPQNLLQVLQKVQLDSSHKQAMMEKVRSYGSVLLSAEEFQKLFNELDRSVVKEHPPRPEYQSPFLQSAQFLFGHYYFDYLGNLIALANLVSICVFLVLDADVLPAERDDFILGILNCVFIVYYLLEMLLKVFALGLRGYLSYPSNVFDGLLTVVLLVLEISTLAVYRLSLWDMTRMLNMLIVFRFLRIIPSMKPMAVVASTVLGLVQNMRAFGGILVVVYYVFAIIGINLFRGVIVALSAPBGSFEQLEYWANNFDDFAAALVTLWNLMVVNNWQVFLDAYRRYSGPWSKIYFVLWWLVSSVIWVNLFLALILENFLHKW
CHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHCCHHHHHHHHHHHHHHCCHHHHCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHCCCHHHHHHCCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHCCCCCCCCCHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCECHHHHHHHHHHCCCCHHHHHHHHHHHHHCCCCCECHHHHHHHHHHHHCCCCCCCCCCCCCCHHHHHHHHHCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHCCCCHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCHHHCCCCCHHHHHHHHHHHHCCCCCHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCC
CCCHHHHHHHHHHHHHHHHHCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHEECCCCHEEHHHHHHHHHHECCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCHHHHHHHHHHHHCCCCCCHHHHHHHHCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCECHHHHHHHHHCCCCCHHHHHHHHHHHHCCCCCCECHHHHHHHHHHHHCCCEECCCCCCHHCCCHHHHHHHHHHCHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCCHHHHHHHHHHHHHHHHHHHHHHHHCHHHHHCCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCHHHHHHHHHHHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCEECCCCCCCCCHHHCCHHHHCHHHHHHHHHHHHHHHHCCCCEEHHHHHHHHCCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCC
> 6dru_1
TYFAPNSTGLRIQHGFETILIQPFGYDGFRVRAWPFRPPSGNEISFIYDPPIEGYEDTAHGMSYDTATTGTEPRTLRNGNIILRTTGWGGTTAGYRLSFYRVNDDGSETLLTNEYAPLKSLNPRYYYWPGPGAEFSAEFSFSATPDEQIYGTGTQQDHMINKKGSVIDMVNFNSYIPTPVFMSNKGYAFIWNMPAEGRMEFGTLRTRFTAASTTLVDYVIVAAQPGDYDTLQQRISALTGRAPAPPDFSLGYIQSKLRYENQTEVELLAQNFHDRNIPVSMIVIDYQSWAHQGDWALDPRLWPNVAQMSARVKNLTGAEMMASLWPSVADDSVNYAALQANGLLSATRDGPGTTDSWNGSYIRNYDSTNPSARKFLWSMLKKNYYDKGIKNFWIDQADGGALGEAYENNGQSTYIESIPFTLPNVNYAAGTQLSVGKLYPWAHQQAIEEGFRNATDTKEGSAADHVSLSRSGYIGSQRFASMIWSGDTTSVWDTLAVQVASGLSAAATGWGWWTVDAGGFEVDSTVWWSGNIDTPEYRELYVRWLAWTTFLPFMRTHGSRTBYFQDAYTBANEPWSYGASNTPIIVSYIHLRYQLGAYLKSIFNQFHLTGRSIMRPLYMDFEKTDPKISQLVSSNSNYTTQQYMFGPRLLVSPVTLPNVTEWPVYLPQTGQNNTKPWTYWWTNETYAGGQVVKVPAPLQHIPVFHLGSREELLSGNVF
CCCCCCCCCEEEEECCEEEEEEECCCCEEEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEECCCCCEEEEECCEEEEEECCCCCCCCCECEEEEECCCCCEEEEEEECCCCCCCCCEEEECCCCCCCEEEEEEEECCCCCCEEEEEECCCCCCCCCCCEEECCCECCEEEEEEEEECCCEEEEECCCCCEEEEECCCEEEEEEEEECCEEEEEEECCCCCHHHHHHHHHHHHCCCCCCCHHHHCEEECCCCCCCHHHHHHHHHHHHHCCCCCCEEEECCCCECCCCCCCECCCCCCCHHHHHHHHHHHHCCEEEEEECCEECCCCCCHHHHHHCCCECEECCCCCCCEEECCEEEEEECCCCHHHHHHHHHHHHHHCHHHCCCEEEECCCCCCCCCCCCCCCCCCHHHHHCCCCCCCEECCCCEHHHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEECCCCCCHHHCCEEEECCCCECCHHHHHHHHHHHHHHHHHCCCCEECCECCCECCCCCCCCCECCCHHHHHHHHHHHHHHCCCCCEEECCCCECCCCCCCCCCCCCCCCCCCCHHHHHHHHHHHHHCHHHHHHHHHHHHHHCCCCEECCHHHHCCCCCCHHHHHHCCCHHHHCCEEECCCEEECCCCCCCCCEEEEEECCCCCCCCCCEEECCCCCEECCCEEEEEECCCCCCCEEECCCHHHHCCCCCC
CCEECCCCEEEEEECCEEEEEEECCCCEEEEEECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEEEEECCCCEEEEECCCEEEEEECCCCCCCCCEEEEEECCCCCCEEEHHHHCCCCCCCCHHHHECCCCCCCEEEEEEEECCCCCCEECCCEECCCCECCCCCEEEEEHCCCCCEEEEEEECCCCEEEECCCCCCEEEEECCCEEEEEEHCCEEEEEEEECCCCCHHHHHHHHHHHHCCCCCCCHHHCCCEEHHHHHCCHHHHHHHHHHHHHCCCCCEEEEEEHCCCCCCCCCCCCCCCCCCHHHHHHHHHHHCCCEEEEEEECCCCCCCHCHHHHHHCCCEEEECCCCCECCCCCCCCEEEECCCCHHHHHHHHHHHHHHHHHHCCEEEEECCCCCCCCCCCCCCCCCCCCCECCCCCCCCEEEECCCHHHHHHHHHHHHHHHHHHHHHHCCCCCCCCCCCEEEEEEECCCHHCCCCEEEECCCCCCCHHHHHHHHHHHHHHHHHCCCHCCCCCCCEECCCCCCCCCCCCCCCCHHHHHHHHHHHHHCCHHECCCCCCCCCCCCCCCCCCEECCCHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHCCCCEEEEEECCCCCCCHHHHCCCCCCCCCCCEEEECCCEEECCCCCCCCEEEEEECCCCCCCCCCEEEECCCCCEECCCCEEEECCCCCECEEEEECCCCCECCCCCC
